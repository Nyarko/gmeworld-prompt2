<div class="col-lg-1 col-md-1 col-sm-0 col-xs-0"></div>
 <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" id="ViewManagerPage">
   <!-- Beginning of form to process data -->
  <div class="form">
  <!-- Beginning of panel in Accounts page -->
  <div class="panel" id="VeiwManagerPanel">
  <!-- Panel Header -->
       <div class="panel-heading" id="ViewManagerPanel_Header"></div>
  <!-- Panel Body -->
       <div class="panel-body" id="ViewManagerPanel_Body">
        <form class="form" role="form" method="form">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="VMName" id="VMName">Name</label>
              <input type="text" class="form-control" name="VMName" id="VMName" data-toggle="tooltip" title="Enter Name">
            </div>
            <div class="form-group">
              <label for="VMGender" id="VMGender">Gender</label>
              <input type="text" class="form-control" name="VMGender" id="VMGender" data-toggle="tooltip" title="Enter Gender">
            </div>
            <div class="form-group">
              <label for="VMnationality" id="VMnationality">Nationality</label>
              <input type="text" class="form-control" name="VMnationality" id="VMnationality" data-toggle="tooltip" title="Enter Nationality">
            </div>
            <div class="form-group">
              <label for="VMcountry">Country</label>
              <input type="text" class="form-control" name="VMcountry" id="VMcountry" data-toggle="tooltip" title="Enter Country">
            </div>
            <div class="form-group">
              <label for="VMregions">Regions/State/Province</label>
              <input type="text" class="form-control" name="VMregions" id="VMregions" data-toggle="tooltip" title="Enter Regions/State/Province">
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="VMcity">City/Town/Village</label>
              <input type="text" class="form-control" name="VMcity" id="VMcity" data-toggle="tooltip" title="Enter City/Town/Village">
            </div>
            <div class="form-group">
              <label for="VMaccountNumber">Account Number</label>
              <input type="text" class="form-control" name="VMaccountNumber" id="VMaccountNumber" data-toggle="tooltip" title="Enter Account Number">
            </div>
            <div class="form-group">
              <label for="VMemailAddress">Email Address</label>
              <input type="text" class="form-control" name="VMemailAddress" id="VMemailAddress" data-toggle="tooltip" title="Enter Email Address">
            </div>
            <div class="form-group">
              <label for="VMmaritalStatus">Marital Status</label>
              <input type="text" class="form-control" name="VMmaritalStatus" id="VMmaritalStatus" data-toggle="tooltip" title="Enter Marital Status">
            </div>
            <div class="form-group">
              <label for="VMpremisesAddress">Premises Address</label>
              <input type="text" class="form-control" name="VMpremisesAddress" id="VMpremisesAddress" data-toggle="tooltip" title="Enter Premises Address">
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="VMidNumber">ID Card Number</label>
              <input type="text" class="form-control" name="VMidNumber" id="VMidNumber" data-toggle="tooltip" title="Enter ID Card Number">
            </div>
            <div class="form-group">
              <label for="VMtinNumber">TIN Number</label>
              <input type="text" class="form-control" name="VMtinNumber" id="VMtinNumber" data-toggle="tooltip" title="Enter TIN Number">
            </div>
            <div class="form-group">
              <label for="VMsigPhonenumber">Signatory Phone Number</label>
              <input type="text" class="form-control" maxlength="12" name="VMsigPhonenumber" id="VMsigPhonenumber" data-toggle="tooltip" title="Enter Signatory Phone Number">
            </div>
          </div>
        </form>
       </div>
  <!-- Panel Footer -->
       <div class="panel-footer" id="ViewManagerPanel_Footer">
       <a href="#" class="btn" name="viewManagerBtn" id="viewManagerSuspendBtn">Suspend</a>
         <a href="#" class="btn" name="viewManagerBtn" id="viewManagerRemoveBtn">Remove</a>
       </div>
  </div> 
  <!-- End of panel in view manager page -->
  </div>
<!-- End of form -->
</div>
<div class="col-lg-1 col-md-1 col-sm-0 col-xs-0"></div>