<div class="col-lg-2 col-md-1 col-sm-0 col-xs-0"></div>
 <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12" id="RemoveBranchPage">
   <!-- Beginning of form to process data -->
  <div class="form">
  <!-- Beginning of panel in Accounts page -->
  <div class="panel" id="RemoveBranchPanel">
  <!-- Panel Header -->
       <div class="panel-heading" id="RemoveBranchPanel_Header">
        
       </div>
  <!-- Panel Body -->
       <div class="panel-body" id="RemoveBranchPanel_Body">
          <div class="col-lg-2 col-md-2 col-sm-0 col-xs-12"></div>
          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="BranchName">Branch Name</label>
              <input type="text" class="form-control" name="BranchName" id="BranchName" data-toggle="tooltip" title="Enter Branch Name">
            </div>
            <div class="form-group">
               <label for="BranchID">Branch ID</label>
               <input type="text" class="form-control" name="BranchID" id="BranchID">
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-0 col-xs-12"></div>
       </div>
  <!-- Panel Footer -->
       <div class="panel-footer" id="RemoveBranchPanel_Footer">
       <!--Button done starts here -->
         <a href="#" class="btn" name="RemoveBranchDone" id="RemoveBranchDone">Done</a>
       </div>
  </div> 
  <!-- End of panel in accounts page -->
  </div>
<!-- End of form -->
</div>
<div class="col-lg-2 col-md-1 col-sm-0 col-xs-0"></div>