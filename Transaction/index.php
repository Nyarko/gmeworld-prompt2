<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Add title to the page -->
  <title>Prompt Network | Dashboard</title>
   <!-- Add icon to the title -->
  <link rel="icon" type="image/png" href="../images/favicon.ico">
   
  <!-- Meta tags link starts here -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Ends here -->
  <!-- Bootstrap links style starts here -->
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <link rel="stylesheet" href="../Bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../Bootstrap/css/bootstrap-theme.min.css">

  <!-- Bootstrap js starts here -->
  <script src="Bootstrap/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
  <script src="../Bootstrap/js/jquery.js"></script>
  <script src="../Bootstrap/js/bootstrap.min.js"></script>

   <!-- Custom css style -->
  <link rel="stylesheet" href="../Bootstrap/css/main.css">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <!-- Css style applied to the main page -->
  <style type="text/css">
    body{
      background:linear-gradient(to left, lavender 83.3%, lavender 16.7% );
      background-image: url("../images/Mosaic.png");
      background-repeat: no-repeat;
    }

    #thePanel{
      margin-top: 50px;
      box-shadow: 5px 0px 5px 5px grey;
    }

    #PanelHead{
      color: ghostwhite;
      letter-spacing: 4px;
      font-style: bolder;
      font-size: 20px;
      background-color: #020D20;
     text-align: center;
     font-family: 'Tahoma';
    }

    #PanelBody{
      font-family: 'Tahoma';
      letter-spacing: 3px;
    }

    #PanelFooter{
      text-align:right;
      font-size: 20px;
      font-family: 'Tahoma';
      letter-spacing: 2px;
    }

    #PanelFooter a{
      text-decoration: none;
      background-color: #020D20;
      color: ghostwhite;
      font-weight: bolder;
    }

     #PanelFooter a:hover{
      color:#BB8E07;
    }

    }

  </style>
 <!-- Ends here -->

</head>
<body>
     <!-- Header navbar starts here -->
    <nav class="navbar navbar-inverse navbar-static-top">
     <div class="container-fluid">
        <div class="navbar-header">
         <a class="navbar-brand" href="index.php" id="title">Pr<span id="lo">o</span>mpt</a>
          <!-- <a class="navbar-brand" href="index.html"><img src="../images/logo2.jpg"></a> -->
        </div>
     </div>  
    </nav>
    <!-- Header navbar ends here -->
     
     <!-- The middle portion -->
    <div class="col-lg-3 col-md-2 col-sm-1 col-xs-12"></div> 

    <div class="col-lg-6 col-md-8 col-sm-10 col-xs-12">
      <!--Beginning of the panel starts here -->
       <div class="panel" id="thePanel">
         <!--
            #### Form Descrption Details ####
             The form contain the following
              - Teller ID with Name (tellerID) & id (tellerID)
              - Branch ID with Name (branchID) & id (branchID)
              - Button to send detail has Name (DoneBtn) & id (DoneBtn)
         -->

          <!-- Form Starts here -->
          <form class="form" method="post" role="form">
          <!-- Panel heading starts here -->
            <div class="panel-heading" id="PanelHead">
               <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">PROVIDE CREDENTIALS TO LOG IN</marquee>
            </div>
            <!-- /end s-->

             <!-- Panel Body Starts here -->
            <div class="panel-body" id="PanelBody">
               <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>

               <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">

                <div class="form-group">
                  <label for="bankName1" id="ManId">Teller ID</label>
                  <input type="text" class="form-control" name="tellerID" id="tellerID" data-toggle="tooltip" title="Enter Teller ID">
                </div>

                <div class="form-group">
                  <label for="bankName1" id="ManId">Branch ID</label>
                  <input type="text" class="form-control" name="branchID" id="branchID" data-toggle="tooltip" title="Enter Branch ID">
                </div>

               </div>
               <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
            </div>
            <!-- /end -->

            <!-- Panel footer starts here -->
            <div class="panel-footer" id="PanelFooter">
              <!-- Button Done Starts here -->
             <a href="transactions.php" class="btn" name="DoneBtn" id="DoneBtn">Done</a>
            </div>
            <!-- /Ends -->
          </form>
          <!--/ Form ends here -->
       </div>
    </div>
    <div class="col-lg-3 col-md-1 col-sm-1 col-xs-12"></div>
    <!-- /End of middle portion -->

     <!-- Start of the footer -->
     <div class="container-fluid">
      <nav class="navbar navbar-inverse navbar-fixed-bottom">
      </nav>
     </div>  
     <!-- End of footer -->
     
     <!-- Custom js -->
     <script src="../Bootstrap/js/main.js"></script>
</body>
</html>                                   
