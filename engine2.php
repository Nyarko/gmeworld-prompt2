<!doctype html>
 <html class="no-js" lang=""> 
    <head>
        <!-- Add title to the page -->
        <title>Prompt Network | Engine</title>
         <!-- Add icon to the title -->
        <link rel="icon" type="image/png" href="images/favicon.ico">
         
         <!-- Meta tags starts here -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <!-- Meta tags end here -->

        <!-- Link of Styling from bootstrap starts here -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="Bootstrap/css/bootstrap-theme.min.css">
        <!-- Custom css style -->
        <link rel="stylesheet" href="Bootstrap/css/custom.css">

        <!-- Some bootstrap script stuff -->
        <script src="Bootstrap/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <!-- 
           ##..... Detail Description of this page ......##

            - The textbox field for Engine Code has Name (engineCode) & id (engineCode)
            - The textbox field for passwoord has Name (pwd) & id (pwd)
            - Button done has Name (done) & id (next)
        -->
    </head>
    <body>
    <!-- Navbar starts here -->
    <!-- THe header portion Starts here -->
     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button id="btnToggle" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only" >Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- The logo tag starts here -->
        <a class="navbar-brand" href="index.php" id="title">Pr<span id="lo">o</span>mpt</a>
        <!--Ends here -->
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <div class="navbar-header">
             <ul class="nav navbar-nav" id="itemNav">
              <!-- Items in the header starts here -->
               <li><a href="main1.php">MAINS</a></li>
               <li class="active"><a href="engine1.php">ENGINE</a></li>
               <!-- Items ends here -->
             </ul>
          </div>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
     <!-- Header portion ends here -->
     <!-- Navbar ends here -->
      <!-- Middle Container divided into col starts here -->
  
        <!--First col starts here -->
         <div class="col-lg-3 col-md-3 col-sm-2 col-xs-12 hidden-xs">
           
         </div>
        <!--First col ends here -->
        <!--Middle col starts here -->
        <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
             <!--Creating the form to collect data starts here-->
             <form class="form" role="form" method="post" action="">
               <!-- Creating of panel starts here -->
                <div class="panel" id="pan">
                   <!-- Panel heading starts here -->
                   <div class="panel-heading" id="connect">
                      <h3>Sign In</h3>
                   </div>
                   <!--Panel Body starts here -->
                    <div class="panel-body" id="pan-body">
                     <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
                     <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                      <div class="form-group">
                       <label for="engineCode">Engine Code</label>
                        <input type="text" class="form-control" name="engineCode" id="engineCode" placeholder="Enter Engine Code" required="required">
                       </div>
                       <div class="form-group">
                       <label for="pwd">Password</label>
                         <input type="password" class="form-control" name="pwd" id="pwd" placeholder="Enter Engine Password" required="required">
                       </div>
                     </div>
                     <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"></div>
                    </div>
                    <!--Panel body ends here -->
                    <!-- Panel footer starts here -->
                    <div class="panel-footer">
                         <div class="form-group">
                            <label class="label-control col-lg-10 col-md-10 col-sm-10 col-xs-9"></label>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                              <!-- Button to send information starts here -->
                              <a href="#" class="btn" name="done" id="next" onclick="location.href = '#';">Done</a>
                               <!-- Ends here -->
                            </div>
                         </div>
                         <div class="clearfix"></div>
                    </div>
                    <!-- Panel footer ends here -->
                </div>
             </form>
        </div>
        <!--Middle col ends here -->
        <!--Last col starts here -->
       <div class="col-lg-3 col-md-3 col-sm-2 col-xs-12 hidden-xs">
         
       </div>
        <!--Last col ends here -->  
  
    <!-- Middle Container divided into col ends here -->
    
    <div class="container-fluid hidden-lg hidden-md">
      <footer class="navbar navbar-inverse navbar-fixed-bottom">
             <a href="engine1.html" id="arrowL" data-toggle="tooltip" title="Go Back">
               <span class="glyphicon glyphicon-circle-arrow-left"></span>
             </a> 
      </footer>
    </div> 
        <!-- Bootstrap stuff starts here -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="Bootstrap/js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="Bootstrap/js/vendor/bootstrap.min.js"></script>

        <script src="Bootstrap/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script> 
   
    </body>
</html>
