<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <!-- Add title to the page -->
    <title>Prompt Network | Setup Configuration</title>

    <!-- Meta tags link starts here -->
    <meta name="robots" content="noindex, nofollow" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <!-- /Ends here -->
     <!-- Add icon to the title -->
    <link rel="icon" type="image/png" href="../images/favicon.ico">

      <!-- Bootstrap style for the multple choice -->
      <link rel="stylesheet" href="multipleBootstrap/example.css" type="text/css">
      <link rel="stylesheet" href="multipleBootstrap/example2.css" type="text/css">
      <link rel="stylesheet" href="multipleBootstrap/example4.css" type="text/css">

      <!-- Bootstrap js for multiple select -->
      <script type="text/javascript" src="multipleBootstrap/exam.js"></script>
      <script type="text/javascript" src="multipleBootstrap/exam1.js"></script>
      <script type="text/javascript" src="multipleBootstrap/exam2.js"></script>

      <link rel="stylesheet" href="multipleBootstrap/multipleselect.css" type="text/css">
      <script type="text/javascript" src="multipleBootstrap/multipleselect.js"></script>
       
       <!-- Custom link -->
      <link rel="stylesheet" href="../Bootstrap/css/custom.css">
   <!-- JS Code to give length to the select currency button -->          
  <script type="text/javascript">
    $(document).ready(function() {
      $('#clickable').multiselect({
        inheritClass: true,
        nonSelectedText: 'Select Currencies',
        enableClickableOptGroups: true,
        enableCollapsibleOptGroups: true,
        enableFiltering: true,
        includeSelectAllOption: true,
        buttonWidth: "auto",
        maxHeight: 350,
        buttonWidth: 300
      });
    });
  </script>
  <!-- JS ends here -->
   <!--
      #### Details on how the code is structured ####
       The below is the Name and id of the varius textbox on the page 

       - Bank Name has Name (bankName) & id (bankName)
       - Password has Name (bankPass) & id (bankPass)
       - Email Address has Name (Email) & id (Email)
       - Management Lock has Name (manLock) & id (manLock)
       - Mains Code has Name (mainsCode) & id (mainsCode)
       - Engine Code has Name (engineCode) & id (engineCode)
       - Type of Bank has Name (bankType) & id (bankType)
       - Country has Name (Country) & id (Country)
       - Currencies Name (multipleSelectDropdown)
   -->
</head>
<body>
     <!-- Navbar starts here -->
     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
           <!-- Logo in the header starts here -->
           <a class="navbar-brand" href="index.php" id="title">Pr<span id="lo">o</span>mpt</a>
           <!-- Ends here -->
        </div>
      </div>
    </nav>
     <!-- Navbar ends here -->
        <!--First column starts here -->
        <div class="col-lg-3 col-md-3 col-sm-1 col-xs-12 hidden-xs">
        </div>

        <!--Middle Column Starts here -->
        <div class="col-lg-6 col-md-6 col-sm-10 col-xs-12">

          <!-- Form that contain the textbox fields starts here -->
         <form class="form" role="form" method="post" action="">
         <!-- Starts of panel -->
         <div class="panel" id="pan">
           <!-- The panel header -->
            <div class="panel-heading" id="connect">
                <h3><b><center><marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Setup</marquee></center></b></h3>
            </div>
            <!-- End of panel header -->
            <!-- Panel body starts here -->
            <div class="panel-body" id="pan-body1">
              <div class="col-lg-2 col-md-1 col-sm-1 col-xs-12"></div>
              <div class="col-lg-8 col-md-10 col-sm-10 col-xs-12">
                   
              <div class="form-group">
               <label for="bankName" class="control-label">Bank Name</label>
                <input type="text" class="form-control" name="bankName" id="bankName" data-toggle="tooltip" title="Enter Bank Name">
              </div>

              <div class="form-group">
                  <label for="bankPass" class="control-label">Password</label>
                   <input type="password" class="form-control" name="bankPass" id="bankPass" data-toggle="tooltip" title="Enter Password">
              </div>

              <div class="form-group">
                  <label for="Email" class="control-label">Email Address</label>
                    <input type="email" class="form-control" name="Email" id="Email" data-toggle="tooltip" title="Enter Email Address">
              </div>

               <div class="form-group">
                <label for="manLock" class="control-label">Management Lock</label>
                   <input type="password" class="form-control" name="manLock" id="manLock" data-toggle="tooltip" title="Enter Management Lock">
              </div>

              <div class="form-group">
                  <label for="mainsCode" class="control-label">Mains Code</label>
                   <input type="text" class="form-control" name="mainsCode" id="mainsCode" data-toggle="tooltip" title="Enter Mains Code">
              </div>

               <div class="form-group">
                <label for="engineCode" class="control-label">Engine Code</label>
                   <input type="text" class="form-control" name="engineCode" id="engineCode" data-toggle="tooltip" title="Enter Engine Code">
              </div>

               <!-- Select bank type dropdown starts here -->
                <div class="form-group">
                 <label for="bankType"  class="control-label">Type of Bank</label>
                  <select class="form-control" name="bankType" id="bankType" data-toggle="tooltip" title="Enter Bank Type">
                      <option selected="selected" value="">Select</option>
                      <option value="OPB">Operator Bank</option>
                      <option value="NNB">National Network Bank</option>
                      <option value="CNB">Continental Network Bank</option>
                      <option value="GNB">Global Network Bank</option>
                  </select>
                 </div>
                <!-- Select bank type ends here -->
                 
                  <!-- Select Country starts here -->
              <div class="form-group">
               <label for="Country"  class="control-label">Country</label>
                  <select class="form-control" name="Country" id="Country" data-toggle="tooltip" title="Choose your Country">
                     <option>Select Country</option>
                        <option value="">A</option>
                        <option value="AF">Afghanistan</option>
                        <option value="AX">Åland Islands</option>
                        <option value="AL">Albania</option>
                        <option value="DZ">Algeria</option>
                        <option value="AS">American Samoa</option>
                        <option value="AD">Andorra</option>
                        <option value="AO">Angola</option>
                        <option value="AI">Anguilla</option>
                        <option value="AQ">Antarctica</option>
                        <option value="AG">Antigua And Barbuda</option>
                        <option value="AR">Argentina</option>
                        <option value="AM">Armenia</option>
                        <option value="AW">Aruba</option>
                        <option value="AU">Australia</option>
                        <option value="AT">Austria</option>
                        <option value="AZ">Azerbaijan</option>
                        <option value="">B</option>
                        <option value="BS">Bahamas</option>
                        <option value="BH">Bahrain</option>
                        <option value="BD">Bangladesh</option>
                        <option value="BB">Barbados</option>
                        <option value="BY">Belarus</option>
                        <option value="BE">Belgium</option>
                        <option value="BZ">Belize</option>
                        <option value="BJ">Benin</option>
                        <option value="BM">Bermuda</option>
                        <option value="BT">Bhutan</option>
                        <option value="BO">Bolivia, Plurinational State Of</option>
                        <option value="BQ">Bonaire, Sint Eustatius And Saba</option>
                        <option value="BA">Bosnia And Herzegovina</option>
                        <option value="BW">Botswana</option>
                        <option value="BV">Bouvet Island</option>
                        <option value="BR">Brazil</option>
                        <option value="IO">British Indian Ocean Territory</option>
                        <option value="BN">Brunei Darussalam</option>
                        <option value="BG">Bulgaria</option>
                        <option value="BF">Burkina Faso</option>
                        <option value="BI">Burundi</option>
                        <option value="">C</option>
                        <option value="KH">Cambodia</option>
                        <option value="CM">Cameroon</option>
                        <option value="CA">Canada</option>
                        <option value="CV">Cape Verde</option>
                        <option value="KY">Cayman Islands</option>
                        <option value="CF">Central African Republic</option>
                        <option value="TD">Chad</option>
                        <option value="CL">Chile</option>
                        <option value="CN">China</option>
                        <option value="CX">Christmas Island</option>
                        <option value="CC">Cocos (Keeling) Islands</option>
                        <option value="CO">Colombia</option>
                        <option value="KM">Comoros</option>
                        <option value="CG">Congo</option>
                        <option value="CD">Congo, The Democratic Republic Of The</option>
                        <option value="CK">Cook Islands</option>
                        <option value="CR">Costa Rica</option>
                        <option value="CI">Côte D'Ivoire</option>
                        <option value="HR">Croatia</option>
                        <option value="CU">Cuba</option>
                        <option value="CW">Curaçao</option>
                        <option value="CY">Cyprus</option>
                        <option value="CZ">Czech Republic</option>
                        <option value="">D</option>
                        <option value="DK">Denmark</option>
                        <option value="DJ">Djibouti</option>
                        <option value="DM">Dominica</option>
                        <option value="DO">Dominican Republic</option>
                        <option value="">E</option>
                        <option value="EC">Ecuador</option>
                        <option value="EG">Egypt</option>
                        <option value="SV">El Salvador</option>
                        <option value="GQ">Equatorial Guinea</option>
                        <option value="ER">Eritrea</option>
                        <option value="EE">Estonia</option>
                        <option value="ET">Ethiopia</option>
                        <option value="">F</option>
                        <option value="FK">Falkland Islands (Malvinas)</option>
                        <option value="FO">Faroe Islands</option>
                        <option value="FJ">Fiji</option>
                        <option value="FI">Finland</option>
                        <option value="FR">France</option>
                        <option value="GF">French Guiana</option>
                        <option value="PF">French Polynesia</option>
                        <option value="TF">French Southern Territories</option>
                        <option value="">G</option>
                        <option value="GA">Gabon</option>
                        <option value="GM">Gambia</option>
                        <option value="GE">Georgia</option>
                        <option value="DE">Germany</option>
                        <option value="GH">Ghana</option>
                        <option value="GI">Gibraltar</option>
                        <option value="GR">Greece</option>
                        <option value="GL">Greenland</option>
                        <option value="GD">Grenada</option>
                        <option value="GP">Guadeloupe</option>
                        <option value="GU">Guam</option>
                        <option value="GT">Guatemala</option>
                        <option value="GG">Guernsey</option>
                        <option value="GN">Guinea</option>
                        <option value="GW">Guinea-Bissau</option>
                        <option value="GY">Guyana</option>
                        <option value="">H</option>
                        <option value="HT">Haiti</option>
                        <option value="HM">Heard Island And Mcdonald Islands</option>
                        <option value="VA">Holy See (Vatican City State)</option>
                        <option value="HN">Honduras</option>
                        <option value="HK">Hong Kong</option>
                        <option value="HU">Hungary</option>
                        <option value="">I</option>
                        <option value="IS">Iceland</option>
                        <option value="IN">India</option>
                        <option value="ID">Indonesia</option>
                        <option value="IR">Iran, Islamic Republic Of</option>
                        <option value="IQ">Iraq</option>
                        <option value="IE">Ireland</option>
                        <option value="IM">Isle Of Man</option>
                        <option value="IL">Israel</option>
                        <option value="IT">Italy</option>
                        <option value="">J</option>
                        <option value="JM">Jamaica</option>
                        <option value="JP">Japan</option>
                        <option value="JE">Jersey</option>
                        <option value="JO">Jordan</option>
                        <option value="">K</option>
                        <option value="KZ">Kazakhstan</option>
                        <option value="KE">Kenya</option>
                        <option value="KI">Kiribati</option>
                        <option value="KP">Korea, Democratic People's Republic Of</option>
                        <option value="KR">Korea, Republic Of</option>
                        <option value="KW">Kuwait</option>
                        <option value="KG">Kyrgyzstan</option>
                        <option value="">L</option>
                        <option value="LA">Lao People's Democratic Republic</option>
                        <option value="LV">Latvia</option>
                        <option value="LB">Lebanon</option>
                        <option value="LS">Lesotho</option>
                        <option value="LR">Liberia</option>
                        <option value="LY">Libya</option>
                        <option value="LI">Liechtenstein</option>
                        <option value="LT">Lithuania</option>
                        <option value="LU">Luxembourg</option>
                        <option value="">M</option>
                        <option value="MO">Macao</option>
                        <option value="MK">Macedonia, The Former Yugoslav Republic Of</option>
                        <option value="MG">Madagascar</option>
                        <option value="MW">Malawi</option>
                        <option value="MY">Malaysia</option>
                        <option value="MV">Maldives</option>
                        <option value="ML">Mali</option>
                        <option value="MT">Malta</option>
                        <option value="MH">Marshall Islands</option>
                        <option value="MQ">Martinique</option>
                        <option value="MR">Mauritania</option>
                        <option value="MU">Mauritius</option>
                        <option value="YT">Mayotte</option>
                        <option value="MX">Mexico</option>
                        <option value="FM">Micronesia, Federated States Of</option>
                        <option value="MD">Moldova, Republic Of</option>
                        <option value="MC">Monaco</option>
                        <option value="MN">Mongolia</option>
                        <option value="ME">Montenegro</option>
                        <option value="MS">Montserrat</option>
                        <option value="MA">Morocco</option>
                        <option value="MZ">Mozambique</option>
                        <option value="MM">Myanmar</option>
                        <option value="">N</option>
                        <option value="NA">Namibia</option>
                        <option value="NR">Nauru</option>
                        <option value="NP">Nepal</option>
                        <option value="NL">Netherlands</option>
                        <option value="NC">New Caledonia</option>
                        <option value="NZ">New Zealand</option>
                        <option value="NI">Nicaragua</option>
                        <option value="NE">Niger</option>
                        <option value="NG">Nigeria</option>
                        <option value="NU">Niue</option>
                        <option value="NF">Norfolk Island</option>
                        <option value="MP">Northern Mariana Islands</option>
                        <option value="NO">Norway</option>
                        <option value="">O</option>
                        <option value="OM">Oman</option>
                        <option value="">P</option>
                        <option value="PK">Pakistan</option>
                        <option value="PW">Palau</option>
                        <option value="PS">Palestine, State Of</option>
                        <option value="PA">Panama</option>
                        <option value="PG">Papua New Guinea</option>
                        <option value="PY">Paraguay</option>
                        <option value="PE">Peru</option>
                        <option value="PH">Philippines</option>
                        <option value="PN">Pitcairn</option>
                        <option value="PL">Poland</option>
                        <option value="PT">Portugal</option>
                        <option value="PR">Puerto Rico</option>
                         <option value="">Q</option>
                        <option value="QA">Qatar</option>
                        <option value="">R</option>
                        <option value="RE">Réunion</option>
                        <option value="RO">Romania</option>
                        <option value="RU">Russian Federation</option>
                        <option value="RW">Rwanda</option>
                        <option value="">S</option>
                        <option value="BL">Saint Barthélemy</option>
                        <option value="SH">Saint Helena, Ascension And Tristan Da Cunha</option>
                        <option value="KN">Saint Kitts And Nevis</option>
                        <option value="LC">Saint Lucia</option>
                        <option value="MF">Saint Martin (French Part)</option>
                        <option value="PM">Saint Pierre And Miquelon</option>
                        <option value="VC">Saint Vincent And The Grenadines</option>
                        <option value="WS">Samoa</option>
                        <option value="SM">San Marino</option>
                        <option value="ST">Sao Tome And Principe</option>
                        <option value="SA">Saudi Arabia</option>
                        <option value="SN">Senegal</option>
                        <option value="RS">Serbia</option>
                        <option value="SC">Seychelles</option>
                        <option value="SL">Sierra Leone</option>
                        <option value="SG">Singapore</option>
                        <option value="SX">Sint Maarten (Dutch Part)</option>
                        <option value="SK">Slovakia</option>
                        <option value="SI">Slovenia</option>
                        <option value="SB">Solomon Islands</option>
                        <option value="SO">Somalia</option>
                        <option value="ZA">South Africa</option>
                        <option value="GS">South Georgia And The South Sandwich Islands</option>
                        <option value="SS">South Sudan</option>
                        <option value="ES">Spain</option>
                        <option value="LK">Sri Lanka</option>
                        <option value="SD">Sudan</option>
                        <option value="SR">Suriname</option>
                        <option value="SJ">Svalbard And Jan Mayen</option>
                        <option value="SZ">Swaziland</option>
                        <option value="SE">Sweden</option>
                        <option value="CH">Switzerland</option>
                        <option value="SY">Syrian Arab Republic</option>
                        <option value="">T</option>
                        <option value="TW">Taiwan</option>
                        <option value="TJ">Tajikistan</option>
                        <option value="TZ">Tanzania, United Republic Of</option>
                        <option value="TH">Thailand</option>
                        <option value="TL">Timor-Leste</option>
                        <option value="TG">Togo</option>
                        <option value="TK">Tokelau</option>
                        <option value="TO">Tonga</option>
                        <option value="TT">Trinidad And Tobago</option>
                        <option value="TN">Tunisia</option>
                        <option value="TR">Turkey</option>
                        <option value="TM">Turkmenistan</option>
                        <option value="TC">Turks And Caicos Islands</option>
                        <option value="TV">Tuvalu</option>
                        <option value="">U</option>
                        <option value="UG">Uganda</option>
                        <option value="UA">Ukraine</option>
                        <option value="AE">United Arab Emirates</option>
                        <option value="GB">United Kingdom</option>
                        <option value="US">United States</option>
                        <option value="UM">United States Minor Outlying Islands</option>
                        <option value="UY">Uruguay</option>
                        <option value="UZ">Uzbekistan</option>
                        <option value="">V</option>
                        <option value="VU">Vanuatu</option>
                        <option value="VE">Venezuela, Bolivarian Republic Of</option>
                        <option value="VN">Viet Nam</option>
                        <option value="VG">Virgin Islands, British</option>
                        <option value="VI">Virgin Islands, U.S.</option>
                        <option value="">W</option>
                        <option value="WF">Wallis And Futuna</option>
                        <option value="EH">Western Sahara</option>
                        <option value="">Y</option>
                        <option value="YE">Yemen</option>
                        <option value="">Z</option>
                        <option value="ZM">Zambia</option>
                        <option value="ZW">Zimbabwe</option>
                      </select>
                  </div>
                  <!-- Select country ends here -->

                 <!-- Select currencies starts here -->
                <div class="form-group">
                <label for="select" class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12">Currencies</label>
                  <select class="form-control col-lg-12 col-md-12 col-sm-12 col-xs-12" id="clickable" multiple="multiple" name="multiSelectDropdown">
                    <optgroup label="A">
                     <option value="AFN">Afghan afghani</option>
                     <option value="ALL">Albanian lek</option>
                     <option value="DZD">Algerian dinar</option>
                     <option value="AOA">Angolan kwanza</option>
                     <option value="ARS">Argentine peso</option>
                     <option value="AMD">Armenian dram</option>
                     <option value="AWG">Aruban florin</option>
                     <option value="AUD">Australian dollar</option>
                     <option value="AZN">Azerbaijani manat</option>
                     <option value="XCD">Antigua and Barbuda</option>
                     <option value="ARS">Argentina</option>
                     <option value="AMD">Armenia</option>
                     <option value="AWG">Aruba (Netherlands)</option>
                     <option value="SHP">Ascension Island (UK)</option>
                     <option value="AUD">Australia</option>
                     <option value="EUR">Austria</option>
                     <option value="AZN">Azerbaijan</option>
                   </optgroup>
                   <optgroup label="B">
                    <option value="BSD">Bahamian dollar</option>
                    <option value="BHD">Bahraini dinar</option>
                    <option value="BDT">Bangladeshi taka</option>
                    <option value="BBD">Barbadian dollar</option>
                    <option value="BYN">Belarusian ruble</option>
                    <option value="BZD">Belize dollar</option>
                    <option value="BMD">Bermudian dollar</option>
                    <option value="BTN">Bhutanese ngultrum</option>
                    <option value="BOB">Bolivian boliviano</option>
                    <option value="BAM">Bosnia and Herzegovina convertible mark</option>
                    <option value="BWP">Botswana pula</option>
                    <option value="BRL">Brazilian real</option>
                    <option value="BND">Brunei dollar</option>
                    <option value="BGN">Bulgarian lev</option>
                    <option value="BIF">Burundi franc</option>
                   </optgroup>
                   <optgroup label="C">
                    <option value="CVE">Cape Verdean escudo</option>
                    <option value="KHR">Cambodian riel</option>
                    <option value="XAF">Central African CFA franc</option>
                    <option value="CAD">Canadian dollar</option>
                    <option value="KYD">Cayman Islands dollar</option>
                    <option value="XAF">Central African CFA franc</option>
                    <option value="CLP">Chilean peso</option>
                    <option value="CNY">Chinese Yuan Renminbi</option>
                    <option value="COP">Colombian peso</option>
                    <option value="KMF">Comorian franc</option>
                    <option value="CDF">Congolese franc</option>
                    <option value="none">Cook Islands dollar</option>
                    <option value="CRC">Costa Rican colon</option>
                    <option value="HRK">Croatian kuna</option>
                    <option value="CUP">Cuban peso</option>
                    <option value="CZK">Czech koruna</option>
                    <option value="XPF">CFP franc</option>
                   </optgroup>
                   <optgroup label="D">
                    <option value="DKK">Danish krone</option>
                    <option value="DJF">Djiboutian franc</option>
                    <option value="DOP">Dominican peso</option>
                    <option value="DKK">Danish krone</option>
                   </optgroup>
                   <optgroup label="E">
                    <option value="EUR">European euro</option>
                    <option value="XCD">East Caribbean dollar</option>
                    <option value="EGP">Egyptian pound</option>
                    <option value="ERN">Eritrean nakfa</option>
                    <option value="ETB">Ethiopian birr</option>
                   </optgroup>
                   <optgroup label="F">
                    <option value="FKP">Falkland Islands pound</option>
                    <option value="none">Faroese krona</option>
                    <option value="FJD">Fijian dollar</option>
                   </optgroup>
                   <optgroup label="G">
                    <option value="GMD">Gambian dalasi</option>
                    <option value="GEL">Georgian lari</option>
                    <option value="GHS">Ghanaian cedi</option>
                    <option value="GIP">Gibraltar pound</option>
                    <option value="GTQ">Guatemalan quetzal</option>
                    <option value="GGP">Guernsey Pound</option>
                    <option value="GNF">Guinean franc</option>
                    <option value="GYD">Guyanese dollar</option>
                   </optgroup>
                   <optgroup label="H">
                    <option value="HTG">Haitian gourde</option>
                    <option value="HNL">Honduran lempira</option>
                    <option value="HKD">Hong Kong dollar</option>
                    <option value="HUF">Hungarian forint</option>
                   </optgroup>
                   <optgroup label="I">
                    <option value="ISK">Icelandic krona</option>
                    <option value="INR">Indian rupee </option>
                    <option value="IDR">Indonesian rupiah</option>
                    <option value="ILS">Israeli new shekel</option>
                    <option value="IRR">Iranian rial</option>
                    <option value="IQD">Iraqi dinar</option>
                   </optgroup>
                   <optgroup label="J">
                    <option value="JMD">Jamaican dollar</option>
                    <option value="JPY">Japanese yen</option>
                    <option value="JEP">Jersey pound</option>
                    <option value="JOD">Jordanian dinar</option>
                   </optgroup>
                   <optgroup label="K">
                    <option value="KZT">Kazakhstani tenge</option>
                    <option value="KES">Kenyan shilling</option>
                    <option value="KWD">Kuwaiti dinar</option>
                    <option value="KGS">Kyrgyzstani som</option>
                   </optgroup>
                   <optgroup label="L">
                    <option value="LAK">Lao kip</option>
                    <option value="LBP">Lebanese pound</option>
                    <option value="LSL">Lesotho loti</option>
                    <option value="LRD">Liberian dollar</option>
                    <option value="LYD">Libyan dinar</option>
                   </optgroup>
                   <optgroup label="M">
                    <option value="IMP">Manx pound </option>
                    <option value="MOP">Macanese pataca</option>
                    <option value="MKD">Macedonian denar</option>
                    <option value="MGA">Malagasy ariary</option>
                    <option value="MWK">Malawian kwacha</option>
                    <option value="MYR">Malaysian ringgit</option>
                    <option value="MVR">Maldivian rufiyaa</option>
                    <option value="MRO">Mauritanian ouguiya</option>
                    <option value="MUR">Mauritian rupee</option>
                    <option value="MXN">Mexican peso</option>
                    <option value="MDL">Moldovan leu</option>
                    <option value="MNT">Mongolian tugrik</option>
                    <option value="MAD">Moroccan dirham</option>
                    <option value="MZN">Mozambican metical</option>
                    <option value="MMK">Myanmar kyat</option>
                   </optgroup>
                   <optgroup label="N">
                    <option value="NZD">New Zealand dollar</option>
                    <option value="ANG">Netherlands Antillean guilder</option>
                    <option value="NAD">Namibian dollar</option>
                    <option value="NPR">Nepalese rupee</option>
                    <option value="TWD">New Taiwan dollar</option>
                    <option value="NIO">Nicaraguan cordoba</option>
                    <option value="NGN">Nigerian naira</option>
                    <option value="KPW">North Korean won</option>
                    <option value="NOK">Norwegian krone</option>
                   </optgroup>
                   <optgroup label="O">
                    <option value="OMR">Omani rial</option>
                   </optgroup>
                   <optgroup label="P">
                    <option value="PKR">Pakistani rupee</option>
                    <option value="PGK">Papua New Guinean kina</option>
                    <option value="PYG">Paraguayan guarani</option>
                    <option value="PEN">Peruvian sol</option>
                    <option value="PHP">Philippine peso</option>
                    <option value="PLN">Polish zloty</option>
                    <option value="GBP">Pound sterling</option>
                   </optgroup>
                   <optgroup label="Q">
                    <option value="QAR">Qatari riyal</option>
                   </optgroup>
                   <optgroup label="R">
                    <option value="RON">Romanian leu</option>
                    <option value="RUB">Russian ruble</option>
                    <option value="RWF">Rwandan franc</option>
                   </optgroup>
                   <optgroup label="S">
                    <option value="SHP">Saint Helena pound</option>
                    <option value="XDR">SDR (Special Drawing Right)</option>
                    <option value="WST">Samoan tala</option>
                    <option value="STD">Sao Tome and Principe dobra</option>
                    <option value="SAR">Saudi Arabian riyal</option>
                    <option value="RSD">Serbian dinar</option>
                    <option value="SCR">Seychellois rupee</option>
                    <option value="SLL">Sierra Leonean leone</option>
                    <option value="SGD">Singapore dollar</option>
                    <option value="SBD">Solomon Islands dollar</option>
                    <option value="SOS">Somali shilling</option>
                    <option value="ZAR">South African rand</option>
                    <option value="KRW">South Korean won</option>
                    <option value="SSP">South Sudanese pound</option>
                    <option value="LKR">Sri Lankan rupee</option>
                    <option value="SDG">Sudanese pound</option>
                    <option value="SRD">Surinamese dollar</option>
                    <option value="SZL">Swazi lilangeni</option>
                    <option value="SEK">Swedish krona</option>
                    <option value="CHF">Swiss franc</option>
                    <option value="SYP">Syrian pound</option>
                   </optgroup>
                   <optgroup label="T">
                    <option value="TJS">Tajikistani somoni</option>
                    <option value="TZS">Tanzanian shilling</option>
                    <option value="THB">Thai baht</option>
                    <option value="TOP">Tongan pa’anga</option>
                    <option value="TTD">Trinidad and Tobago dollar</option>
                    <option value="TND">Tunisian dinar</option>
                    <option value="TRY">Turkish lira</option>
                    <option value="TMT">Turkmen manat</option>
                   </optgroup>
                   <optgroup label="U">
                    <option value="USD">United States dollar</option>
                    <option value="UGX">Ugandan shilling</option>
                    <option value="UAH">Ukrainian hryvnia</option>
                    <option value="AED">UAE dirham</option>
                    <option value="UYU">Uruguayan peso</option>
                    <option value="UZS">Uzbekistani som</option>
                   </optgroup>
                   <optgroup label="V">
                    <option value="VUV">Vanuatu vatu</option>
                    <option value="VEF">Venezuelan bolivar</option>
                    <option value="VND">Vietnamese dong</option>
                   </optgroup>
                   <optgroup label="W">
                    <option value="XOF">West African CFA franc</option>
                   </optgroup>
                   <optgroup label="Y">
                    <option value="YER">Yemeni rial</option>
                   </optgroup>
                   <optgroup label="Z">
                    <option value="ZMW">Zambian kwacha</option>
                   </optgroup>
                 </select>
               </div>
              <!-- Select currencies end here -->
              </div>
              <div class="col-lg-2 col-md-1 col-sm-1 col-xs-12"></div>
            </div>
            <!-- Panel body ends here -->
            <!-- Panel Footer starts here -->
            <div class="panel-footer">
                <label class="label-control col-lg-10 col-md-10 col-sm-10 col-xs-9"></label>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                  <a class="btn" name="next" id="next" onclick="location.href = 'welcome.php';">Done</a>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- Panel footer ends here -->
         </div>
       </form>
       <!-- Creating of form ends here -->
      </div>  
      <!-- Last Column starts here -->
      <div class="col-lg-3 col-md-3 col-sm-1 col-xs-12 hidden-xs">
       
      </div> 
     <!-- Footer Starts here -->
   <div class="container-fluid hidden-lg hidden-md">
      <footer class="navbar navbar-inverse navbar-fixed-bottom" id="botNav">     
      </footer>
    </div> 
    <!-- Footer ends here -->
    <!-- /container -->  

    <!-- The js that help display a message when the mouse pointer is on the text feilds -->
    <script>
       $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>
    <!-- Ends here -->                       
</body>
</html>

















