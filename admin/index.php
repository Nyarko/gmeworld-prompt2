<!doctype html>
 <html class="no-js" lang=""> 
    <head>
        <!-- Add title to the page -->
        <title>Prompt Network | Dashboard</title>
         <!-- Add icon to the title -->
        <link rel="icon" type="image/png" href="../images/favicon.ico">
         
         <!-- The metas tags -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
         <!-- Styling Links -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="../Bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../Bootstrap/css/bootstrap-theme.min.css">
        <!-- Custom css -->
        <link rel="stylesheet" href="../Bootstrap/css/customMain.css">
        <script src="Bootstrap/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
    <!-- Navbar starts here -->
     <nav class="navbar navbar-inverse navbar-static-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button id="btnToggle" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only" >Toggle navigation</span>
            <span  class="icon-bar"></span>
            <span  class="icon-bar"></span>
            <span  class="icon-bar"></span>
          </button>
          <div class="navbar-brand">
          <a href="#" class="hidden-lg hidden-md hidden-sm" onclick='openNav1()' title='Menu' id="Menu">&#9776;</a>
         </div>
         <a class="navbar-brand" href="index.php" id="title">Pr<span id="lo">o</span>mpt</a>
          <!-- <a class="navbar-brand" href="index.html"><img src="../images/logo2.jpg"></a> -->
        </div>
        <div id="navbar" class="navbar-collapse collapse">
             <ul class="nav navbar-nav" id="itemNav">
               <li  class="active"><a href="index.php"><img src="../images/controller.png" id="iConH"> CONTROLLER</a></li>
               <li><a href="capture.php"><img src="../images/capture.png" id="iConH"> CAPTURE</a></li>
             </ul>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
     <!-- Navbar ends here -->
      <!-- Middle Container divided into col starts here -->
    <div class="container-fluid" id="mainContainer">
        <!--First col starts here -->
    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-0" id="sideBar">
      <ul class="nav nav-stacked">
      <li class="divider" style="color:black; font-size: 30px;" id="CloseX">
        <a href="#SideMenu" class="hidden-lg hidden-md hidden-sm" onclick='closeNav1()'>&times;</a>
      </li>
         <li><a data-toggle="pill" href="#createAccount"><img src="../images/Account.png" id="iCon"> Open Account</a></li>
         <li><a data-toggle="pill" href="#viewAccount"><img src="../images/view.png" id="iCon"> View Account</a></li>
         <li><a data-toggle="pill" href="#deposit"><img src="../images/moni.png" id="iCon">  Deposit</a></li>
         <li><a data-toggle="pill" href="#withDraw"><img src="../images/withdraw.png" id="iCon">   Withdraw</a></li>
         <li><a data-toggle="pill" href="#checkBalance"><img src="../images/check.png" id="iCon">  Check Balance</a></li>
         <li><a data-toggle="pill" href="#suspendAccount"><img src="../images/suspend.png" id="iCon"> Suspend Account</a></li>
     </ul>
    </div>
        <!--First col ends here -->
        <!--Middle col starts here -->
          <div class="tab-content col-lg-10 col-md-10 col-sm-9 col-xs-12" id="main">

           <!--==============================================
               The Login aspect of the pages
           ================================================-->
           <!-- The welcome page starts here -->
            <div class="tab-pane fade in active">
             <div class="col-lg-2 col-md-1 col-sm-0 col-xs-12"></div>
             <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                <div class="panel" id="welComeController_Panel">
                     <!--Welcome panel header starts here -->
                      <div class="panel-heading" id="connect">
                         <h3>Welcome to <strong>PROMPT</strong> Controller</h3>
                      </div>
                      <div class="panel-body" id="welComeController_Panel_Body">
                      <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
                      <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12" id="Middle">
                        <p><marquee>
                         <span class="glyphicon glyphicon-chevron-left"></span> <label>Select from the Menu</label>
                        </marquee></p>
                      </div>
                      <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
                      </div>
                      <div class="panel-footer">
                        
                      </div>
                </div>
             </div>
             <div class="col-lg-2 col-md-1 col-sm-0 col-xs-12"></div>
            </div>
        


             <!--=======================================================
                  The main Pages of the controller menu
             =========================================================-->
            <!--Deposit page starts here -->
            <div id="deposit" class="tab-pane fade in">
              <?php include '../includes/controllerDeposit.php'; ?>
            </div>

            <!-- Withdraw page start from here -->
            <div id="withDraw" class="tab-pane fade">
              <?php include '../includes/controllerWithdraw.php'; ?>
            </div>

            <!-- Controller Check Balance main page -->
            <div id="checkBalance" class="tab-pane fade">
             <?php include '../includes/controllerCheckbalance.php'; ?>
            </div>

            <!-- Controller Open Account Main Pages -->
            <div id="createAccount" class="tab-pane fade">
              <?php include '../includes/controllerOpenaccount.php'; ?>
            </div>
            
            <!-- View Account Main Page Starts here -->
            <div id="viewAccount" class="tab-pane fade">
              <?php include '../includes/controllerViewaccount.php'; ?>
            </div>

            <!-- Suspend page starts here -->
            <div id="suspendAccount" class="tab-pane fade">
              <?php include '../includes/controllerSuspendaccount.php'; ?>
            </div>       
        <!--Middle col ends here -->   
    </div>
    <!-- Middle Container divided into col ends here -->
     <div class="container-fluid hidden-lg hidden-md">
      <footer class="navbar navbar-inverse navbar-fixed-bottom">
            <p style="text-align:center" >&copy; Prompt Network 2017</p>
      </footer>
    </div> <!-- /container -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="../Bootstrap/js/vendor/jquery-1.11.2.js"><\/script>')</script>
        <script src="../Bootstrap/js/vendor/bootstrap.min.js"></script>

        <script src="../Bootstrap/js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>

    </body>
</html>
