 <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0"></div>
 <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" id="AddManagerPage">
   <!-- Beginning of form to process data -->
  <div class="form">
  <!-- Beginning of panel in Accounts page -->
  <div class="panel" id="AddManagerPanel">
  <!-- Panel Header -->
       <div class="panel-heading" id="AddManagerPanel_Header"></div>
  <!-- Panel Body -->
       <div class="panel-body" id="AddManagerPanel_Body">
        <form class="form" role="form" method="form">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="Name">Name</label>
              <input type="text" class="form-control" name="Name" id="Name" data-toggle="tooltip" title="Enter Name">
            </div>
            <div class="form-group">
              <label for="Gender">Gender</label>
              <input type="text" class="form-control" name="Gender" id="Gender" data-toggle="tooltip" title="Enter Gender">
            </div>
            <div class="form-group">
              <label for="nationality">Nationality</label>
              <input type="text" class="form-control" name="nationality" id="nationality" data-toggle="tooltip" title="Enter Nationality">
            </div>
            <div class="form-group">
              <label for="country">Country</label>
              <input type="text" class="form-control" name="country" id="country" data-toggle="tooltip" title="Enter Country">
            </div>
            <div class="form-group">
              <label for="regions">Regions/State/Province</label>
              <input type="text" class="form-control" name="regions" id="regions" data-toggle="tooltip" title="Enter Regions/State/Province">
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="city">City/Town/Village</label>
              <input type="text" class="form-control" name="city" id="city" data-toggle="tooltip" title="Enter City/Town/Village">
            </div>
            <div class="form-group">
              <label for="accountNumber">Account Number</label>
              <input type="text" class="form-control" name="accountNumber" id="accountNumber" data-toggle="tooltip" title="Enter Account Number">
            </div>
            <div class="form-group">
              <label for="emailAddress">Email Address</label>
              <input type="text" class="form-control" name="emailAddress" id="emailAddress" data-toggle="tooltip" title="Enter Email Address">
            </div>
            <div class="form-group">
              <label for="maritalStatus">Marital Status</label>
              <input type="text" class="form-control" name="maritalStatus" id="maritalStatus" data-toggle="tooltip" title="Enter Marital Status">
            </div>
            <div class="form-group">
              <label for="premisesAddress">Premises Address</label>
              <input type="text" class="form-control" name="premisesAddress" id="premisesAddress" data-toggle="tooltip" title="Enter Premises Address">
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="idNumber">ID Card Number</label>
              <input type="text" class="form-control" name="idNumber" id="idNumber" data-toggle="tooltip" title="Enter ID Card Number">
            </div>
            <div class="form-group">
              <label for="tinNumber">TIN Number</label>
              <input type="text" class="form-control" name="tinNumber" id="tinNumber" data-toggle="tooltip" title="Enter TIN Number">
            </div>
            <div class="form-group">
              <label for="sigPhonenumber">Signatory Phone Number</label>
              <input type="text" class="form-control" maxlength="12" name="sigPhonenumber" id="sigPhonenumber" data-toggle="tooltip" title="Enter Signatory Phone Number">
            </div>
            <div class="form-group">
              <label for="branchID">Branch ID</label>
              <input type="text" class="form-control" maxlength="12" name="branchID" id="branchID" data-toggle="tooltip" title="Enter Branch ID">
            </div>
          </div>
        </form>
       </div>
  <!-- Panel Footer -->
       <div class="panel-footer" id="AddManagerPanel_Footer">
         <a href="#" class="btn" name="addManagerBtn" id="addManagerBtn">Done</a>
       </div>
  </div> 
  <!-- End of panel in accounts page -->
  </div>
<!-- End of form -->
</div>
<div class="col-lg-1 col-md-1 col-sm-0 col-xs-0"></div>