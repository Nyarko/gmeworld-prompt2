<div class="col-lg-2 col-md-1 col-sm-0 col-xs-0"></div>
 <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12" id="ChangeLockPage">
   <!-- Beginning of form to process data -->
  <div class="form">
  <!-- Beginning of panel in Accounts page -->
  <div class="panel" id="ChangeLockPanel">
  <!-- Panel Header -->
       <div class="panel-heading" id="ChangeLockPanel_Header">
        
       </div>
  <!-- Panel Body -->
       <div class="panel-body" id="ChangeLockPanel_Body">
          <div class="col-lg-2 col-md-2 col-sm-0 col-xs-12"></div>
          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="CMLock" id="CMLock">Current Management Lock</label>
              <input type="text" class="form-control" name="CMLock" id="CMLock" data-toggle="tooltip" title="Enter Current Management Lock">
            </div>
            <div class="form-group">
              <label for="NMLock" id="NMLock">New Management Lock</label>
              <input type="text" class="form-control" name="NMLock" id="NMLock" data-toggle="tooltip" title="Enter New Management Lock">
            </div>
            <div class="form-group">
              <label for="RNMLock">Re-type New Management Lock</label>
              <input type="text" class="form-control" name="RNMLock" id="RNMLock" data-toggle="tooltip" title="Enter New Management Lock">
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-0 col-xs-12"></div>
       </div>
  <!-- Panel Footer -->
       <div class="panel-footer" id="ChangeLockPanel_Footer">
       <!--Button done starts here -->
         <a href="#" class="btn" name="ChangeLockDone" id="ChangeLockDone">Done</a>
       </div>
  </div> 
  <!-- End of panel in accounts page -->
  </div>
<!-- End of form -->
</div>
<div class="col-lg-2 col-md-1 col-sm-0 col-xs-0"></div>