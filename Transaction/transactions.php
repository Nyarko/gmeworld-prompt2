<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Add title to the page -->
  <title>Prompt Network | Transactions</title>

   <!-- Add icon to the title -->
  <link rel="icon" type="image/png" href="../images/favicon.ico">

  <!-- Meta tags links starts here -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- /Ends here -->

  <!-- Bootstrap css style link starts here -->
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <link rel="stylesheet" href="../Bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../Bootstrap/css/bootstrap-theme.min.css">
  <!-- /ends here -->
  
  <!-- Bootstrap js links starts here -->
  <script src="Bootstrap/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
  <script src="../Bootstrap/js/jquery.js"></script>
  <script src="../Bootstrap/js/bootstrap.min.js"></script>
  <!-- / Ends here -->
  
  <!-- Custom css -->
  <link rel="stylesheet" href="../Bootstrap/css/main.css">


  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <!-- Affix Style starts here -->
  <style>
  .affix {
      top:20px;
      margin-left:-7px;
      margin-top: -20px;
      overflow: auto;
  }
  </style>
  <!-- /Ends here -->

</head>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="15">

<!-- Navbar or header starts here -->
<nav class="navbar navbar-inverse navbar-static-top">
 <div class="container-fluid">
    <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> 
    </button>
   
      <div class="navbar-brand">
        <a href="#" class="hidden-lg hidden-md hidden-sm" onclick='openNav()' title='Menu' id="Menu">&#9776;</a>
     </div>
     <a class="navbar-brand" href="index.php" id="title">Pr<span id="lo">o</span>mpt</a>
      <!-- <a class="navbar-brand" href="index.html"><img src="../images/logo2.jpg"></a> -->
    </div>
    <div class="collapse navbar-collapse" id="navbar">
         <ul class="nav navbar-nav" id="itemNav">
            <!-- List of Items in header -->
           <li><a href="index.php" class="active"><img src="../images/Transaction_48px.png" id="iConH"> TRANSACTIONS</a></li>
           <li><a href="control_login.php"><img src="../images/Control_48px.png" id="iConH"> CONTROL ROOM</a></li>
            <!-- List of items ends here -->
         </ul>
    </div>
    <!--/.navbar-collapse -->
 </div>  
</nav>
<!-- /Navbar Ends here -->

<!-- Middle container starts here -->
<div class="container-fluid" id="myScrollspy">

  <nav class="col-lg-2 col-md-2 col-sm-3 col-xs-0" id="SideMenu">
    <!-- This show the side navbar which contain Deposits and Withdrawals -->
    <ul class="nav nav-pills nav-stacked" data-spy="affix" data-offset-top="50">
       <!-- A Closing button for closing the sidebar at small screen -->
      <li class="divider" style="color:black; font-size: 30px;" id="CloseX">
        <a href="#SideMenu" class="hidden-lg hidden-md hidden-sm" onclick='closeNav()'>&times;</a>
      </li>
       <!-- Closing button ends here -->
       <!-- Items in sidebar starts here -->
      <li><a data-toggle="pill" href="#deposits"><img src="../images/Investment_48px.png" id="iConH"> DEPOSITS</a></li>
      <li><a data-toggle="pill" href="#withdrawals"><img src="../images/Withdrawal_48px.png" id="iConH"> WITHDRAWALS</a></li>
       <!-- /Items in sidebar ends here -->
    </ul>
   <!-- /Ends here -->
  </nav>

   <!-- Main tabs for pages starts here -->
  <div class="tab-content col-lg-10 col-md-10 col-sm-9 col-xs-12">
      <!--
      ####......Welcome Page Starts here......#####
    -->
     <div class="tab-pane fade in active" id="firstPage">
          <div class="col-lg-2 col-md-2 col-sm-0 col-xs-0"></div>
          <div class="col-lg-8 col-md-8 col-sm-0 col-xs-0" id="FirstPage">
             <div class="panel" id="FirstPagePanel">
                  <div class="panel-heading" id="FirstPagePanel_Header">
                    <p>Welcome to <strong>PROMPT</strong> Transactions</p>
                  </div>
                  <div class="panel-body" id="FirstPagePanel_Body">
                  <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
                  <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12" id="Middle">
                     <p><marquee><span class="glyphicon glyphicon-chevron-left"></span> <label>Select from the Menu</label></marquee></p>
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
                  </div>
                  <div class="panel-footer" id="FirstPagePanel_Footer"></div>
             </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-0 col-xs-0"></div>
      </div> 

   <!--The whole page for deposit starts here -->
    <div class="tab-pane fade in" id="deposits">    
      <div class="col-lg-2 col-md-2 col-sm-1 col-xs-12"></div>
      <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12">
        <!-- Panel starts here -->
        <div class="panel" id="DepositsPane">
          <?php include '../includes/deposit.php';?>
        </div>
        <!-- End of panel -->
      </div>
      <div class="col-lg-2 col-md-2 col-sm-1 col-xs-12"></div>
    </div>

    <!-- The whole page for withdrawal starts here -->
    <div class="tab-pane fade in" id="withdrawals"> 
       <?php include '../includes/withdrawal.php';?>
    </div>       
  </div>
  </div>
  <!-- /Middle Container Ends here -->

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
  <script>window.jQuery || document.write('<script src="../Bootstrap/js/vendor/jquery-1.11.2.js"><\/script>')</script>
  <script src="../Bootstrap/js/vendor/bootstrap.min.js"></script>

  <!-- Custom js -->
  <script src="../Bootstrap/js/main.js"></script>

  <!-- Helps in the collapsing and expanding of the navbar at media querie -->
  <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X','auto');ga('send','pageview');
  </script>
       
</body>
</html>                                   
