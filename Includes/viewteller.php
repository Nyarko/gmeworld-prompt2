<div class="col-lg-1 col-md-1 col-sm-0 col-xs-0"></div>
   <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" id="ViewTellerPage">
     <!-- Beginning of form to process data -->
    <div class="form">
    <!-- Beginning of panel in Accounts page -->
    <div class="panel" id="ViewTellerPanel">
    <!-- Panel Header -->
         <div class="panel-heading" id="ViewTellerPanel_Header"></div>
    <!-- Panel Body -->
         <div class="panel-body" id="ViewTellerPanel_Body">
          <form class="form" role="form" method="form">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="form-group">
                <label for="VTName">Name</label>
                <input type="text" class="form-control" name="VTName" id="VTName" data-toggle="tooltip" title="Enter Name">
              </div>
              <div class="form-group">
                <label for="VTGender">Gender</label>
                <input type="text" class="form-control" name="VTGender" id="VTGender" data-toggle="tooltip" title="Enter Gender">
              </div>
              <div class="form-group">
                <label for="VTnationality">Nationality</label>
                <input type="text" class="form-control" name="VTnationality" id="VTnationality" data-toggle="tooltip" title="Enter Nationality">
              </div>
              <div class="form-group">
                <label for="VTAddTcountry">Country</label>
                <input type="text" class="form-control" name="VTAddTcountry" id="VTAddTcountry" data-toggle="tooltip" title="Enter Country">
              </div>
              <div class="form-group">
                <label for="VTregions">Regions/State/Province</label>
                <input type="text" class="form-control" name="VTregions" id="VTAddTregions" data-toggle="tooltip" title="Enter Regions/State/Province">
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="form-group">
                <label for="VTcity">City/Town/Village</label>
                <input type="text" class="form-control" name="VTcity" id="VTcity" data-toggle="tooltip" title="Enter City/Town/Village">
              </div>
              <div class="form-group">
                <label for="VTaccountNumber">Account Number</label>
                <input type="text" class="form-control" name="VTaccountNumber" id="VTaccountNumber" data-toggle="tooltip" title="Enter Account Number">
              </div>
              <div class="form-group">
                <label for="VTemailAddress">Email Address</label>
                <input type="text" class="form-control" name="VTemailAddress" id="VTemailAddress" data-toggle="tooltip" title="Enter Email Address">
              </div>
              <div class="form-group">
                <label for="VTmaritalStatus">Marital Status</label>
                <input type="text" class="form-control" name="VTmaritalStatus" id="VTmaritalStatus" data-toggle="tooltip" title="Enter Marital Status">
              </div>
              <div class="form-group">
                <label for="VTpremisesAddress">Premises Address</label>
                <input type="text" class="form-control" name="VTpremisesAddress" id="VTpremisesAddress" data-toggle="tooltip" title="Enter Premises Address">
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="form-group">
                <label for="VTidNumber">ID Card Number</label>
                <input type="text" class="form-control" name="VTidNumber" id="VTidNumber" data-toggle="tooltip" title="Enter ID Card Number">
              </div>
              <div class="form-group">
                <label for="VTtinNumber">TIN Number</label>
                <input type="text" class="form-control" name="VTtinNumber" id="VTtinNumber" data-toggle="tooltip" title="Enter TIN Number">
              </div>
              <div class="form-group">
                <label for="VTsigPhonenumber">Signatory Phone Number</label>
                <input type="text" class="form-control" maxlength="12" name="VTsigPhonenumber" id="VTsigPhonenumber" data-toggle="tooltip" title="Enter Signatory Phone Number">
              </div>
            </div>
          </form>
         </div>
    <!-- Panel Footer -->
         <div class="panel-footer" id="ViewTellerPanel_Footer">
          <a href="#" class="btn" name="viewTellerBtnSuspend" id="viewTellerBtnSuspend">Suspend</a>
           <a href="#" class="btn" name="viewTellerBtnRemove" id="viewTellerBtnRemove">Remove</a>
         </div>
    </div> 
    <!-- End of panel in accounts page -->
    </div>
  <!-- End of form -->
  </div>
  <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0"></div>