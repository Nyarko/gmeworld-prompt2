 <!--=====================================================
           ####......Add Branch Main page......####
           - Branch Name has Name (branchName) & id (branchName)
           - Branch ID has Name (branchID) & id (branchID)
  ========================================================-->
<div class="col-lg-2 col-md-1 col-sm-0 col-xs-0"></div>
 <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12" id="AddBranchPage">
   <!-- Beginning of form to process data -->
  <div class="form" role="form" method="post" action="">
  <!-- Beginning of panel in Accounts page -->
  <div class="panel" id="AddBranchPanel">
  <!-- Panel Header -->
       <div class="panel-heading" id="AddBranchPanel_Header">
        
       </div>
  <!-- Panel Body -->
       <div class="panel-body" id="AddBranchPanel_Body">
          <div class="col-lg-2 col-md-2 col-sm-0 col-xs-12"></div>
          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
             <div class="form-group">
                <label for="branchName" class="control-label">Branch Name</label>
                <input type="text" class="form-control" name="branchName" id="branchName" placeholder="Enter Branch Name">
             </div>
             <div class="form-group">
                <label for="branchID" class="control-label">Branch ID</label>
                <input type="text" class="form-control" name="branchID" id="branchID" placeholder="Enter Branch ID">
             </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-0 col-xs-12"></div>
       </div>
  <!-- Panel Footer -->
       <div class="panel-footer" id="AddBranchPanel_Footer">
       <!--Button done starts here -->
         <a href="#" class="btn" name="AddBranchDone" id="AddBranchDone">Done</a>
       </div>
  </div> 
  <!-- End of panel in accounts page -->
  </div>
<!-- End of form -->
</div>
<div class="col-lg-2 col-md-1 col-sm-0 col-xs-0"></div>