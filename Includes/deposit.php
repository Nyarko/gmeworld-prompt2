<!-- Panel Header -->
<div class="panel-heading" id="DepositsPane_Head" style="color: white; font-family: Tahoma; font-size: 25px; font-weight: bolder; letter-spacing: 3px">
   <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Deposits</marquee>
</div>
<!-- /End of panel header -->

<!-- Starts of panel body -->
<div class="panel-body" id="DepositsPane_Body">
    <!-- Alert to display information starts here -->
    <div class="alert" id="withDrawalDisplay">
      <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
        <!-- Form to display info starts here -->
        <form class="form" role="form">
         <!--Display information in alert starts here -->
         <!-- Customer name display starts her -->
         <p><strong>Customer Name: </strong> Nyarko Isaac</p>
         <!-- Customer account number is display here -->
         <p><strong>Account Number: </strong> 768945433</p>
         <!--Customer Amount is display here -->
         <p><strong>Amount: </strong> GHS234</p>
         <!-- Customer token is display here -->
         <p><strong>Token: </strong> DRH235HR</p>
       </form>
       <!-- /End of form -->
      </div>
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <center>
           <!-- CheckBox to close alert starts here -->
          <a href="#" class="close" data-dismiss="alert" aria-label="close">
           <input type="checkbox" name="chk">
          </a>
          <!-- /Ends -->
         </center>
      </div>
      <div class="clearfix"></div>
    </div>
    <!-- /Alert Ends here -->
</div>
<!-- End of panel body -->

 <!-- Start of panel footer -->
<div class="panel-footer" id="DepositsPane_Footer"></div>
<!-- /End of footer -->