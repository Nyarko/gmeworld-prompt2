<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Add title to the page -->
  <title>Prompt Network | Dashboard</title>
   <!-- Add icon to the title -->
  <link rel="icon" type="image/png" href="../images/favicon.ico">
  <!-- The meta tag starts here -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- /End -->
   <!-- Bootstrap stuff starts here-->
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <link rel="stylesheet" href="../Bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../Bootstrap/css/bootstrap-theme.min.css">
  <script src="Bootstrap/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

  <link rel="stylesheet" href="../Bootstrap/css/main.css">
  <script src="../Bootstrap/js/jquery.js"></script>
  <script src="../Bootstrap/js/bootstrap.min.js"></script>

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <!-- Style for affix starts here -->
  <style>
  .affix {
      top:20px;
      margin-left:-7px;
      margin-top: -20px;
      overflow: auto;
  }

  @media (min-width: 767px) {
    .affix {
      background: #C4C4C4;
      height:600px;
      position: fixed;
      max-width: 200px;
      padding-left: 4px;
      padding-right: 4px;
  }

  }
 @media (max-width: 767px) {
  .affix{
    z-index: 1;
    width: 170px;
    margin-left:0px;
    position: absolute;
    opacity:1.0;
}

}
  </style>
  <!-- Style ends here -->
</head>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="15">
<!-- Header starts here -->
<nav class="navbar navbar-inverse navbar-static-top">
 <div class="container-fluid">
    <div class="navbar-header">
    <button id="btnToggle" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
           <span class="sr-only" >Toggle navigation</span>
           <span  class="icon-bar"></span>
           <span  class="icon-bar"></span>
           <span  class="icon-bar"></span>
        </button>
     <div class="navbar-brand">
        <a href="#" class="hidden-lg hidden-md hidden-sm" onclick='openNav()' title='Menu' id="Menu">&#9776;</a>
    </div>
      <a class="navbar-brand" href="index.php" id="title">Pr<span id="lo">o</span>mpt</a>
      <!-- <a class="navbar-brand" href="index.html"><img src="../images/logo2.jpg"></a> -->
    </div>
    <div id="navbar" class="navbar-collapse collapse">
         <ul class="nav navbar-nav" id="itemNav">
           <!-- List of item in header starts here -->
           <li><a href="index.php" ><img src="../images/Transaction_48px.png" id="iConH"> TRANSACTIONS</a></li>
           <li><a href="control_login.php" class="active"><img src="../images/Control_48px.png" id="iConH"> CONTROL ROOM</a></li>
           <!-- /Ends here -->
         </ul>
    </div>
    <!--/.navbar-collapse -->
 </div>  
</nav>
<!-- /Ends here -->
<div class="container-fluid" id="myScrollspy">
     <!-- Sidebar starts here -->
    <nav class="col-lg-2 col-md-2 col-sm-3 col-xs-1" id="SideMenu">
      <ul class="nav nav-pills nav-stacked" data-spy="affix" data-offset-top="50">
       <!-- Button to close the sidebar at media screen -->
        <li class="divider" style="color:black; font-size: 30px;" id="CloseX">
        <a href="#SideMenu" class="hidden-lg hidden-md hidden-sm" onclick='closeNav()'>&times;</a>
        </li>
        <!-- /End here -->
        <!-- List of item in the asidebar starts here -->
        <li><a data-toggle="pill" href="#accountLogin"><img src="../images/Accounting_48px.png" id="iConH"> ACCOUNTS</a></li>
        <li><a data-toggle="pill" href="#changeLock"><img src="../images/Lock_48px.png" id="iConH"> CHANGE LOCK</a></li>
        <li><a data-toggle="pill" href="#addBranch"><img src="../images/Sakura_48px.png" id="iConH"> ADD BRANCH</a></li>
        <li><a data-toggle="pill" href="#removeBranch"><img src="../images/Remove_48px.png" id="iConH"> REMOVE BRANCH</a></li>
        <li><a data-toggle="pill" href="#addManager"><img src="../images/Manager_48px.png" id="iConH"> ADD MANAGER</a></li>
        <li><a data-toggle="pill" href="#viewManager"><img src="../images/Eye_48px.png" id="iConH"> VIEW MANAGER</a></li>
        <li><a data-toggle="pill" href="#addTeller"><img src="../images/Teller_48px.png" id="iConH"> ADD TELLER</a></li>
        <li><a data-toggle="pill" href="#viewTeller"><img src="../images/Eye_48px.png" id="iConH"> VIEW TELLER</a></li>
        <li><a data-toggle="pill" href="#Tellers"><img src="../images/Teller_48px.png" id="iConH"> TELLER</a></li>
        <li><a data-toggle="pill" href="#OwnAccountPane"><img src="../images/Customer_48px.png" id="iConH"> OWN ACCOUNTS</a></li>
        <li><a data-toggle="pill" href="#HighValuePane"><img src="../images/Plot_48px.png" id="iConH"> HIGH VALUE</a></li>
        <!-- End of list of items in sidebar -->
        <li><a data-toggle="pill" href="#"></a></li>
      </ul>
    </nav>
     <!-- /Ends here -->
    <div class="tab-content col-lg-10 col-md-10 col-sm-9 col-xs-11">

    <!--
      ####......Welcome Page Starts here......#####
    -->
     <div class="tab-pane fade in active" id="firstPage">
          <div class="col-lg-2 col-md-2 col-sm-0 col-xs-0"></div>
          <div class="col-lg-8 col-md-8 col-sm-0 col-xs-0" id="FirstPage">
             <div class="panel" id="FirstPagePanel">
                  <div class="panel-heading" id="FirstPagePanel_Header">
                    <p>Welcome to <strong>PROMPT</strong> Control Room</p>
                  </div>
                  <div class="panel-body" id="FirstPagePanel_Body">
                  <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
                  <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12" id="Middle">
                     <p><marquee><span class="glyphicon glyphicon-chevron-left"></span> <label>Select from the Menu</label></marquee></p>
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-1 col-xs-0"></div>
                  </div>
                  <div class="panel-footer" id="FirstPagePanel_Footer"></div>
             </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-0 col-xs-0"></div>
      </div> 



      <!--
        ####.....Accounts login page starts here.....#####
          ...Details on the Page..
          - Manager Password has name (managerPass) & id (managerPass)
          - Branch ID has name (branchID) & id (branchID)
          - Button has name (AccountLoginBtn) & id (AccountLoginBtn)
      -->
      <div class="tab-pane fade in" id="accountLogin">    
         <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="AccountLoginPage">
           <!-- Beginning of form to process data -->
          <div class="form" role="form" method="post" action="">
          <!-- Beginning of panel for Accounts login page -->
          <div class="panel" id="AccountLoginPanel">
           <!-- Panel Header -->
               <div class="panel-heading" id="AccountLoginPanel_Header">
                   <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Enter Accounts Credentials</marquee>
               </div>
            <!-- Panel Body -->
               <div class="panel-body" id="AccountLoginPanel_Body">
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
                   <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                   
                      <div class="form-group">
                        <label for="managerPass" id="ManId">Manager Password</label>
                        <input type="password" class="form-control" name="managerPass" id="managerPass" data-toggle="tooltip" title="Enter Manager ID">
                      </div>
                      
                      <div class="form-group">
                        <label for="bankName1" id="ManId">Branch ID</label>
                        <input type="text" class="form-control" name="branchID" id="branchID" data-toggle="tooltip" title="Enter Branch ID">
                      </div>
                      
                   </div>
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
               </div>
             <!-- Panel Footer -->
               <div class="panel-footer" id="AccountLoginPanel_Footer">
                 <!-- <button type="button" class="btn btn-default btn-lg btn-block" name="AccountLoginBtn" id="AccountLoginBtn" onclick="location.href = 'control.html';"><strong>Log In</strong></button> -->
                  <strong>
                  <a data-toggle="pill" href="#AccountPane" class="btn btn-default btn-block" name="AccountLoginBtn" id="AccountLoginBtn">Log in</a>
                  </strong> 
               </div>
            <!-- Panel footer ends here -->
          </div> 
          <!-- End of panel in accounts page -->
          </div>
        <!-- End of form -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
      </div>




       <!--
           ####......Change lock Login page starts here....#####
            .... Detail description
             - Management Lock has Name (ManagementLock) & id (ManagementLock)
             - Password  has Name (ManagementPassword) & id (ManagementPassword)
             - Button Login has Name (ChangeLockLoginBtn) & id (ChangeLockLoginBtn)
       -->

      <div class="tab-pane fade in" id="changeLock"> 
         <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="ChangeLockLoginPage">

           <!-- Beginning of form to process data -->
          <form class="form" role="form" method="post" action="">
          <!-- Beginning of panel for Accounts login page -->
          <div class="panel" id="ChangeLockLoginPanel">
           <!-- Panel Header -->
               <div class="panel-heading" id="ChangeLockLoginPanel_Header">
                   <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Enter Change Lock Credentials</marquee>
               </div>
            <!-- Panel header Ends here -->
            <!-- Panel Body -->
               <div class="panel-body" id="ChangeLockLoginPanel_Body">
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
                   <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">

                      <div class="form-group">
                        <label for="bankName1" id="ManId">Management Lock</label>
                        <input type="text" class="form-control" name="ManagementLock" id="ManagementLock" data-toggle="tooltip" title="Enter Management Lock">
                      </div>

                      <div class="form-group">
                        <label for="bankName1" id="ManId">Password</label>
                        <input type="password" class="form-control" name="ManagementPassword" id="ManagementPassword" data-toggle="tooltip" title="Enter Management Password">
                      </div>

                   </div>
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
               </div>
             <!-- Panel body ends here -->
             <!-- Panel Footer -->
               <div class="panel-footer" id="ChangeLockLoginPanel_Footer">
                 <!-- <button type="button" class="btn btn-default btn-lg btn-block" name="AccountLoginBtn" id="AccountLoginBtn" onclick="location.href = 'control.html';"><strong>Log In</strong></button> -->
                 <!-- Button to Login starts here -->
                  <strong>
                  <a data-toggle="pill" href="#ChangeLock" class="btn btn-default btn-block" name="ChangeLockLoginBtn" id="ChangeLockLoginBtn">Log in</a>
                  </strong> 
                  <!-- /Ends here -->
               </div>
          </div> 
          <!-- End of panel in accounts page -->
          </form>
        <!-- End of form -->

        </div>
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
      </div>




        <!--
         ####........Add branch Login page starts here........#
         .. Page Details 
           - Management Lock has Name (ManagementLock) & id (ManagementLock)
       -->

      <div class="tab-pane fade in" id="addBranch"> 
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="AddBranchLoginPage">
           <!-- Beginning of form to process data -->
          <div class="form">
          <!-- Beginning of panel for Add branch login page -->
          <div class="panel" id="AddBranchLoginPanel">
           <!-- Panel Header -->
               <div class="panel-heading" id="AddBranchLoginPanel_Header">
                <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Enter Add Branch Credentials</marquee>
               </div>
            <!-- Panel Header ends here -->
            <!-- Panel Body -->
               <div class="panel-body" id="AddBranchLoginPanel_Body">
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
                   <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">

                      <div class="form-group">
                        <label for="bankName1" id="ManId">Management Lock</label>
                        <input type="text" class="form-control" name="ManagementLock" id="ManagementLock" data-toggle="tooltip" title="Enter Management Lock">
                      </div>

                   </div>
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
               </div>
              <!-- Panel Body Ends here -->
              <!-- Panel Footer -->
               <div class="panel-footer" id="AddBranchLoginPanel_Footer">
                 <!-- <button type="button" class="btn btn-default btn-lg btn-block" name="AccountLoginBtn" id="AccountLoginBtn" onclick="location.href = 'control.html';"><strong>Log In</strong></button> -->
                 <!-- Button to login starts here -->
                  <strong>
                  <a data-toggle="pill" href="#AddBranchPane" class="btn btn-default btn-block" name="AddBranchLoginBtn" id="AddBranchLoginBtn">Log in</a>
                  </strong>
                  <!-- Button ends here --> 
               </div>
          </div> 
          <!-- End of panel in add branch page -->
          </div>
        <!-- End of form -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
      </div> 




      <!-- 
        #####......Remove branch page starts here.....######
         .....Detail Description 
         - Management Lock Name (ManagementLock) & id (ManagementLock)
         - Button to login Name (RemoveBranchLoginBtn) & id (RemoveBranchLoginBtn)
      -->
      <div class="tab-pane fade in" id="removeBranch"> 
         <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="RemoveBranchLoginPage">
           <!-- Beginning of form to process data -->
          <div class="form">
          <!-- Beginning of panel for Remove branch login page -->
          <div class="panel" id="RemoveBranchLoginPanel">
           <!-- Panel Header -->
            <div class="panel-heading" id="RemoveBranchLoginPanel_Header">
              <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Enter Remove Branch Credentials</marquee>
            </div>
            <!-- Panel Body -->
               <div class="panel-body" id="RemoveBranchLoginPanel_Body">
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
                   <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label for="bankName1" id="ManId">Management Lock</label>
                        <input type="text" class="form-control" name="ManagementLock" id="ManagementLock" data-toggle="tooltip" title="Enter Management Lock">
                      </div>
                   </div>
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
               </div>
             <!-- Panel Footer -->
               <div class="panel-footer" id="RemoveBranchLoginPanel_Footer">
                 <!-- <button type="button" class="btn btn-default btn-lg btn-block" name="AccountLoginBtn" id="AccountLoginBtn" onclick="location.href = 'control.html';"><strong>Log In</strong></button> -->
                 <!-- Button to login -->
                  <strong>
                  <a data-toggle="pill" href="#RemoveBranchPane" class="btn btn-default btn-block" name="RemoveBranchLoginBtn" id="RemoveBranchLoginBtn">Log in</a>
                  </strong>
                  <!-- /End --> 
               </div>
          </div> 
          <!-- End of panel in remove branch page -->
          </div>
        <!-- End of form -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
      </div> 




      <!-- 
          ####.....Add Manager Page starts Here.....####
          ... Detail Description of page
          - Management Lock has Name (ManagementLock) id (ManagementLock)
          - Button Log in has Name (AddManagerLoginBtn) id (AddManagerLoginBtn)
      -->

      <div class="tab-pane fade in" id="addManager"> 
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="AddManagerLoginPage">
           <!-- Beginning of form to process data -->
          <div class="form" role="form" method="post" action="">
          <!-- Beginning of panel for Remove branch login page -->
          <div class="panel" id="AddManagerLoginPanel">
           <!-- Panel Header -->
            <div class="panel-heading" id="AddManagerLoginPanel_Header">
              <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Enter Add Manager Credentials</marquee>
            </div>
            <!-- Panel Body -->
               <div class="panel-body" id="AddManagerLoginPanel_Body">
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
                   <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">

                      <div class="form-group">
                        <label for="bankName1" id="ManId">Management Lock</label>
                        <input type="text" class="form-control" name="ManagementLock" id="ManagementLock" data-toggle="tooltip" title="Enter Management Lock">
                      </div>

                   </div>
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
               </div>
             <!-- Panel Footer -->
               <div class="panel-footer" id="AddManagerLoginPanel_Footer">
                 <!-- <button type="button" class="btn btn-default btn-lg btn-block" name="AccountLoginBtn" id="AccountLoginBtn" onclick="location.href = 'control.html';"><strong>Log In</strong></button> -->
                 <!-- Button to Log in starts here -->
                  <strong>
                   <a data-toggle="pill" href="#AddManagerPane" class="btn btn-default btn-block" name="AddManagerLoginBtn" id="AddManagerLoginBtn">Log in</a>
                  </strong>
                  <!-- /End of button --> 
               </div>
          </div> 
          <!-- End of panel in remove branch page -->
          </div>
        <!-- End of form -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
      </div> 



      <!--
        ####.....View Manager Page Starts here.....#####
        ... Detail Description
          - Management Lock has Name (ManagementLock) & id (ManagementLock)
          - Button Login has Name (ViewManagerLoginBtn) & id (ViewManagerLoginBtn)
      -->
      <div class="tab-pane fade in" id="viewManager"> 
         <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="ViewManagerLoginPage">
           <!-- Beginning of form to process data -->
          <div class="form" role="form" method="POST" action="">
          <!-- Beginning of panel for view manager login page -->
          <div class="panel" id="ViewManagerLoginPanel">
           <!-- Panel Header -->
            <div class="panel-heading" id="ViewManagerLoginPanel_Header">
              <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Enter View Manager Credentials</marquee>
            </div>
            <!-- Panel Body -->
               <div class="panel-body" id="ViewManagerLoginPanel_Body">
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
                   <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">

                      <div class="form-group">
                        <label for="bankName1" id="ManId">Management Lock</label>
                        <input type="text" class="form-control" name="ManagementLock" id="ManagementLock" data-toggle="tooltip" title="Enter Management Lock">
                      </div>

                   </div>
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
               </div>
             <!-- Panel Footer -->
               <div class="panel-footer" id="ViewManagerLoginPanel_Footer">
                 <!-- <button type="button" class="btn btn-default btn-lg btn-block" name="AccountLoginBtn" id="AccountLoginBtn" onclick="location.href = 'control.html';"><strong>Log In</strong></button> -->
                 <!-- Button log in starts here -->
                  <strong>
                  <a data-toggle="pill" href="#ViewManagerPane" class="btn btn-default btn-block" name="ViewManagerLoginBtn" id="ViewManagerLoginBtn">Log in</a>
                  </strong> 
                <!-- /Ends here -->
               </div>
          </div> 
          <!-- End of panel in view manager page -->
          </div>
        <!-- End of form -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
      </div> 




       <!--
        ###......Add Teller Page Starts here.....###
        ... Detail Description
         - Manager ID has Name (managerID) & id (ManagerID)
         - Branch ID has Name (branchID) & id (branchID)
         - Button has Name (AddTellerLoginBtn) & id (AddTellerLoginBtn)
      -->

      <div class="tab-pane fade in" id="addTeller"> 
         <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="AddTellerLoginPage">
           <!-- Beginning of form to process data -->
          <div class="form" role="" method="post" action=""> 
          <!-- Beginning of panel for add teller login page -->
          <div class="panel" id="AddTellerLoginPanel">
           <!-- Panel Header -->
            <div class="panel-heading" id="AddTellerLoginPanel_Header">
              <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Enter Add Teller Credentials</marquee>
            </div>
           <!-- /Ends -->
            <!-- Panel Body -->
               <div class="panel-body" id="AddTellerLoginPanel_Body">
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
                   <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">

                     <div class="form-group">
                        <label for="bankName1" id="ManId">Manager ID</label>
                        <input type="text" class="form-control" name="managerID" id="managerID" data-toggle="tooltip" title="Enter Manager ID">
                      </div>

                      <div class="form-group">
                        <label for="bankName1" id="ManId">Branch ID</label>
                        <input type="text" class="form-control" name="branchID" id="branchID" data-toggle="tooltip" title="Enter Branch ID">
                      </div>

                   </div>
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
               </div>
             <!-- Panel Footer -->
               <div class="panel-footer" id="AddTellerLoginPanel_Footer">
                 <!-- <button type="button" class="btn btn-default btn-lg btn-block" name="AccountLoginBtn" id="AccountLoginBtn" onclick="location.href = 'control.html';"><strong>Log In</strong></button> -->
                  <strong>
                  <a data-toggle="pill" href="#AddTellerPane" class="btn btn-default btn-block" name="AddTellerLoginBtn" id="ViewManagerLoginBtn">Log in</a>
                  </strong> 
               </div>
          </div> 
          <!-- End of panel in add teller page -->
          </div>
        <!-- End of form -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
      </div> 




      <!--
        ###....View Teller Page Starts here.....###
        ... Detail Description
         - Manager ID has Name (managerID) & id (ManagerID)
         - Branch ID has Name (branchID) & id (branchID)
         - Button has Name (ViewTellerLoginBtn) & id (ViewTellerLoginBtn) 
      -->

      <div class="tab-pane fade in" id="viewTeller">
         <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="ViewTellerLoginPage">
           <!-- Beginning of form to process data -->
          <div class="form" role="form" method="post" action="">
          <!-- Beginning of panel for view Teller login page -->
          <div class="panel" id="ViewTellerLoginPanel">
          <!-- Panel Header -->
               <div class="panel-heading" id="ViewTellerLoginPanel_Header">
                <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Enter View Teller Credentials</marquee>
               </div>
              <!-- /End -->
             <!-- Panel Body -->
               <div class="panel-body" id="ViewTellerLoginPanel_Body">
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
                   <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">

                      <div class="form-group">
                        <label for="bankName1" id="ManId">Manager ID</label>
                        <input type="text" class="form-control" name="managerID" id="managerID" data-toggle="tooltip" title="Enter Manager ID">
                      </div>

                      <div class="form-group">
                        <label for="bankName1" id="ManId">Branch ID</label>
                        <input type="text" class="form-control" name="branchID" id="branchID" data-toggle="tooltip" title="Enter Branch ID">
                      </div>

                   </div>
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
               </div>
              <!-- Panel Footer -->
               <div class="panel-footer" id="ViewTellerLoginPanel_Footer">
                 <!-- <button type="button" class="btn btn-default btn-lg btn-block" name="AccountLoginBtn" id="AccountLoginBtn" onclick="location.href = 'control.html';"><strong>Log In</strong></button> -->
                 <!-- Button Login starts here -->
                  <strong>
                  <a data-toggle="pill" href="#ViewTellerPane" class="btn btn-default btn-block" name="ViewTellerLoginBtn" id="ViewTellerLoginBtn">Log in</a>
                  </strong> 
                  <!-- /Ends here -->
               </div>
          </div> 
          <!-- End of panel in view teller page -->
          </div>
        <!-- End of form -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
      </div> 




      <!--
        ####.....The CTR Control Main Pages....####
         ... Detail description
          - Management Lock has Name (CRTManageLock) & id (CRTManageLock)
          - Butoon Name (CRTControlLoginBtn) & id (CRTControlLoginBtn)
      -->

      <div class="tab-pane fade in" id="Tellers"> 
         <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="CRTControlLoginPage">
           <!-- Beginning of form to process data -->
          <div class="form" role="form" method="POST" action="">
          <!-- Beginning of panel for view Teller login page -->
          <div class="panel" id="CRTControlLoginPanel">
          <!-- Panel Header -->
               <div class="panel-heading" id="CRTControlLoginPanel_Header">
                <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Enter Tellers Credentials</marquee>
               </div>
          <!-- Panel Body -->
               <div class="panel-body" id="CRTControlLoginPanel_Body">
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
                   <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                   
                      <div class="form-group">
                        <label for="CRTManageLock" id="CRTManageLock">Management Lock</label>
                        <input type="text" class="form-control" name="CRTManageLock" id="CRTManageLock" data-toggle="tooltip" title="Enter Manager ID">
                      </div>

                   </div>
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
               </div>
          <!-- Panel Footer -->
               <div class="panel-footer" id="CRTControlLoginPanel_Footer">
                 <!-- <button type="button" class="btn btn-default btn-lg btn-block" name="AccountLoginBtn" id="AccountLoginBtn" onclick="location.href = 'control.html';"><strong>Log In</strong></button> -->
                  <strong><a data-toggle="pill" href="#CRTControlPane" class="btn btn-default btn-block" name="CRTControlLoginBtn" id="CRTControlLoginBtn">Log in</a></strong> 
               </div>
          </div> 
          <!-- End of panel in view teller page -->
          </div>
        <!-- End of form -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
      </div> 



       <!--
        ####.....The Own Accounts Main Pages....####
         ... Detail description
         
      -->

      <div class="tab-pane fade in" id="OwnAccountPane"> 
         <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="TellersLoginPage">
           <!-- Beginning of form to process data -->
          <div class="form" role="form" method="POST" action="">
          <!-- Beginning of panel for view Teller login page -->
          <div class="panel" id="TellersLoginPanel">
          <!-- Panel Header -->
               <div class="panel-heading" id="TellersLoginPanel_Header">
                <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Enter Own Accounts Credentials</marquee>
               </div>
          <!-- Panel Body -->
               <div class="panel-body" id="TellersLoginPanel_Body">
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
                   <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                   
                      <div class="form-group">
                        <label for="CRTManageLock" id="CRTManageLock">Management Lock</label>
                        <input type="text" class="form-control" name="CRTManageLock" id="CRTManageLock" data-toggle="tooltip" title="Enter Manager ID">
                      </div>

                   </div>
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
               </div>
          <!-- Panel Footer -->
               <div class="panel-footer" id="TellersLoginPanel_Footer">
                 <!-- <button type="button" class="btn btn-default btn-lg btn-block" name="AccountLoginBtn" id="AccountLoginBtn" onclick="location.href = 'control.html';"><strong>Log In</strong></button> -->
                  <strong>
                  <a data-toggle="pill" href="#OwnAccount" class="btn btn-default btn-block" name="CRTControlLoginBtn" id="CRTControlLoginBtn">Log in</a>
                  </strong> 
               </div>
          </div> 
          <!-- End of panel in view teller page -->
          </div>
        <!-- End of form -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
      </div> 


      <!--
        ####.....The Own Accounts Main Pages....####
         ... Detail description
         
      -->

      <div class="tab-pane fade in" id="HighValuePane"> 
         <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="TellersLoginPage">
           <!-- Beginning of form to process data -->
          <div class="form" role="form" method="POST" action="">
          <!-- Beginning of panel for view Teller login page -->
          <div class="panel" id="TellersLoginPanel">
          <!-- Panel Header -->
               <div class="panel-heading" id="TellersLoginPanel_Header">
                <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Enter High Value Credentials</marquee>
               </div>
          <!-- Panel Body -->
               <div class="panel-body" id="TellersLoginPanel_Body">
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
                   <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                   
                      <div class="form-group">
                        <label for="CRTManageLock" id="CRTManageLock">Management Lock</label>
                        <input type="text" class="form-control" name="CRTManageLock" id="CRTManageLock" data-toggle="tooltip" title="Enter Manager ID">
                      </div>

                   </div>
                   <div class="col-lg-2 col-md-10 col-sm-12 col-xs-12"></div>
               </div>
          <!-- Panel Footer -->
               <div class="panel-footer" id="TellersLoginPanel_Footer">
                 <!-- <button type="button" class="btn btn-default btn-lg btn-block" name="AccountLoginBtn" id="AccountLoginBtn" onclick="location.href = 'control.html';"><strong>Log In</strong></button> -->
                  <strong>
                  <a data-toggle="pill" href="#HighValue" class="btn btn-default btn-block" name="CRTControlLoginBtn" id="CRTControlLoginBtn">Log in</a>
                  </strong> 
               </div>
          </div> 
          <!-- End of panel in view teller page -->
          </div>
        <!-- End of form -->
        </div>
        <div class="col-lg-3 col-md-3 col-sm-0 col-xs-0"></div>
      </div> 



     <!--======================================
        The main pages starts here
     =========================================-->
     
      <!-- Account page starts here -->
     <div class="tab-pane fade in" id="AccountPane"> 
        <?php include '../includes/account.php'; ?>
     </div>

      <!-- Add Branch Starts here -->
     <div class="tab-pane fade in" id="AddBranchPane"> 
         <?php include '../includes/addbranch.php'; ?>
      </div>

    <!--==============================
           Remove Branch Main page
        ================================-->
     <div class="tab-pane fade in" id="RemoveBranchPane"> 
          <?php include '../includes/removebranch.php'; ?>
     </div>
  
       <!--==============================
           Change Lock Main page
        ================================-->
     <div class="tab-pane fade in" id="ChangeLock"> 
         <?php include '../includes/changelock.php'; ?>
     </div>

      <!--==============================
           Add Manager main page
        ================================-->
     <div class="tab-pane fade in" id="AddManagerPane"> 
        <?php include '../includes/addmanager.php'; ?>
      </div>

       <!--==============================
           View Manager main page

           VM = View Manager
        ================================-->
     <div class="tab-pane fade in" id="ViewManagerPane"> 
         <?php include '../includes/viewmanager.php'; ?>
     </div>

       <!--==============================
           Add Teller Main Page
        ================================-->
     <div class="tab-pane fade in" id="AddTellerPane"> 
          <?php include '../includes/addteller.php'; ?>
     </div>


       <!--==============================
           View Teller Main Page
           VT = View Teller
           Starts here
        ================================-->
     <div class="tab-pane fade in" id="ViewTellerPane"> 
         <?php include '../includes/viewteller.php'; ?>
     </div>



      <!--==============================
           Tellers Main page
        ================================-->
     <div class="tab-pane fade in" id="CRTControlPane"> 
         <?php include '../includes/teller.php'; ?>
     </div>

     <!--==============================
           Own Account Main page
        ================================-->
     <div class="tab-pane fade in" id="OwnAccount"> 
         <?php include '../includes/ownaccount.php'; ?>
     </div>
  

   <!--==============================
           High Value Main page
        ================================-->
     <div class="tab-pane fade in" id="HighValue"> 
         <?php include '../includes/highvalue.php'; ?>
     </div>
  
     </div>
      <!-- End of Pane -->
    </div>
    <!-- End of container -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="../Bootstrap/js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="../Bootstrap/js/vendor/bootstrap.min.js"></script>
        <!-- Applying tooltip styling in script -->
        <script src="../Bootstrap/js/main.js"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
</body>
</html>                                   
