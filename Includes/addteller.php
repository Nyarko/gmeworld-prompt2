 <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0"></div>
 <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" id="AddTellerPage">
   <!-- Beginning of form to process data -->
  <div class="form">
  <!-- Beginning of panel in Accounts page -->
  <div class="panel" id="AddTellerPanel">
  <!-- Panel Header -->
       <div class="panel-heading" id="AddTellerPanel_Header"></div>
  <!-- Panel Body -->
       <div class="panel-body" id="AddTellerPanel_Body">
        <form class="form" role="form" method="form">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="AddTName">Name</label>
              <input type="text" class="form-control" name="AddTName" id="AddTName" data-toggle="tooltip" title="Enter Name">
            </div>
            <div class="form-group">
              <label for="AddTGender">Gender</label>
              <input type="text" class="form-control" name="AddTGender" id="AddTGender" data-toggle="tooltip" title="Enter Gender">
            </div>
            <div class="form-group">
              <label for="AddTnationality">Nationality</label>
              <input type="text" class="form-control" name="AddTnationality" id="AddTnationality" data-toggle="tooltip" title="Enter Nationality">
            </div>
            <div class="form-group">
              <label for="AddTcountry">Country</label>
              <input type="text" class="form-control" name="AddTcountry" id="AddTcountry" data-toggle="tooltip" title="Enter Country">
            </div>
            <div class="form-group">
              <label for="AddTregions">Regions/State/Province</label>
              <input type="text" class="form-control" name="AddTregions" id="AddTregions" data-toggle="tooltip" title="Enter Regions/State/Province">
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="AddTcity">City/Town/Village</label>
              <input type="text" class="form-control" name="AddTcity" id="AddTcity" data-toggle="tooltip" title="Enter City/Town/Village">
            </div>
            <div class="form-group">
              <label for="AddTaccountNumber">Account Number</label>
              <input type="text" class="form-control" name="AddTaccountNumber" id="AddTaccountNumber" data-toggle="tooltip" title="Enter Account Number">
            </div>
            <div class="form-group">
              <label for="AddTemailAddress">Email Address</label>
              <input type="text" class="form-control" name="AddTemailAddress" id="AddTemailAddress" data-toggle="tooltip" title="Enter Email Address">
            </div>
            <div class="form-group">
              <label for="AddTmaritalStatus">Marital Status</label>
              <input type="text" class="form-control" name="AddTmaritalStatus" id="AddTmaritalStatus" data-toggle="tooltip" title="Enter Marital Status">
            </div>
            <div class="form-group">
              <label for="AddTpremisesAddress">Premises Address</label>
              <input type="text" class="form-control" name="AddTpremisesAddress" id="AddTpremisesAddress" data-toggle="tooltip" title="Enter Premises Address">
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="AddTidNumber">ID Card Number</label>
              <input type="text" class="form-control" name="AddTidNumber" id="AddTidNumber" data-toggle="tooltip" title="Enter ID Card Number">
            </div>
            <div class="form-group">
              <label for="AddTtinNumber">TIN Number</label>
              <input type="text" class="form-control" name="AddTtinNumber" id="AddTtinNumber" data-toggle="tooltip" title="Enter TIN Number">
            </div>
            <div class="form-group">
              <label for="sigPhonenumber">Signatory Phone Number</label>
              <input type="text" class="form-control" maxlength="12" name="AddTsigPhonenumber" id="AddTsigPhonenumber" data-toggle="tooltip" title="Enter Signatory Phone Number">
            </div>
            <div class="form-group">
              <label for="AddTbranchID">Branch ID</label>
              <input type="text" class="form-control" maxlength="12" name="AddTbranchID" id="AddTbranchID" data-toggle="tooltip" title="Enter Branch ID">
            </div>
          </div>
        </form>
       </div>
  <!-- Panel Footer -->
       <div class="panel-footer" id="AddTellerPanel_Footer">
         <a href="#" class="btn" name="addTellerBtn" id="addTellerBtn">Done</a>
       </div>
  </div> 
  <!-- End of panel in accounts page -->
  </div>
<!-- End of form -->
</div>
<div class="col-lg-1 col-md-1 col-sm-0 col-xs-0"></div>