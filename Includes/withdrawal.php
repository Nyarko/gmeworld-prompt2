<div class="col-lg-2 col-md-2 col-sm-1 col-xs-12"></div>
  <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12">
    <!-- Starts of panel -->
    <div class="panel" id="withDrawalPane">
      <!-- Starts of panel heading -->
      <div class="panel-heading" id="withDrawalPane_Head" style="color: white; font-family: Tahoma; font-size: 25px; font-weight: bolder; letter-spacing: 3px">
         <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">Withdrawals</marquee>
      </div>
      <!-- /Panel Heading Ends here -->
      <!-- Panel Body Ends here -->
      <div class="panel-body" id="withDrawalPane_Body">
          <!-- Alert to display information starts here -->
         <div class="alert" id="withDrawalDisplay">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
              <!-- Form to display info starts here -->
              <form class="form" role="form">
               <!--Display information in alert starts here -->
               <!-- Customer name display starts her -->
               <p><strong>Customer Name: </strong> Stephen Kyei</p>
               <!-- Customer account number is display here -->
               <p><strong>Account Number: </strong> 768945433</p>
               <!--Customer Amount is display here -->
               <p><strong>Amount: </strong> GHS234</p>
               <!-- Customer token is display here -->
               <p><strong>Token: </strong> DRH235HR</p>
             </form>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
              <center>
               <!-- CheckBox for closing alert of information starts here -->
              <a href="#" class="close" data-dismiss="alert" aria-label="close">
                <input type="checkbox" name="chk" style="background-color: red">
              </a>
              <!-- Ends here -->
              </center>
            </div>
            <div class="clearfix"></div>
          </div>
          <!-- / Alert End Here -->
      </div>
      <!-- /Ends of panel Body -->
      <!-- Panel Footer -->
      <div class="panel-footer" id="withDrawalPane_Footer"></div>
      <!-- /Ends -->
    </div>
    <!--/ End of panel -->
  </div>
  <div class="col-lg-2 col-md-2 col-sm-1 col-xs-12"></div>