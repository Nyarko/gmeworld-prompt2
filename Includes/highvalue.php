<div class="col-lg-2 col-md-1 col-sm-0 col-xs-0"></div>
 <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12" id="CRTControlPage">
   <!-- Beginning of form to process data -->
  <div class="form">
  <!-- Beginning of panel in Accounts page -->
  <div class="panel" id="CRTControlPanel">
  <!-- Panel Header -->
       <div class="panel-heading" id="DepositsPane_Head" style="color: white; font-family: Tahoma; font-size: 25px; font-weight: bolder; letter-spacing: 3px">
            <marquee direction="right" behavior="alternate" hspace="20%" scrollamount="1">High Value</marquee>
       </div>
  <!-- Panel Body -->
       <div class="panel-body" id="CRTControlPanel_Body">
          <div class="col-lg-2 col-md-2 col-sm-0 col-xs-12"></div>
          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="form-group">
              <label for="Ghcedi">Ghana Cedi</label>
              <input type="text" class="form-control" name="Ghcedi" id="Ghcedi">
            </div>
            <div class="form-group">
              <label for="AmDollar">American Dollar</label>
              <input type="text" class="form-control" name="AmDollar" id="AmDollar">
            </div>
            <div class="form-group">
              <label for="Pound">Pound</label>
              <input type="text" class="form-control" name="Pound" id="Pound">
            </div>
             <div class="form-group">
              <label for="Euro">Euro</label>
              <input type="text" class="form-control" name="Euro" id="Euro">
            </div>
            <div class="form-group">
              <label for="Yuan">Yuan Remnebi</label>
              <input type="text" class="form-control" name="Yuan" id="Yuan">
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-0 col-xs-12"></div>
       </div>
  <!-- Panel Footer -->
       <div class="panel-footer" id="CRTControlPanel_Footer">
       <!--Button done starts here -->
         <a href="#" class="btn" name="CRTControlDone" id="CRTControlDone">Done</a>
       </div>
  </div> 
  <!-- End of panel in accounts page -->
  </div>
<!-- End of form -->
</div>
<div class="col-lg-2 col-md-1 col-sm-0 col-xs-0"></div>