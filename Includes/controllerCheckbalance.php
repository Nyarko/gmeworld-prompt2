<!--First column in check balance page -->
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 hidden-sm hidden-xs"> 
</div>
<!-- Middle column on check balance -->
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
      <!-- Check Balance panel starts from here -->
      <div class="panel" id="checkBalancePanel">
          <div class="panel-heading" id="connect">
            <h3><strong><center>Check Balance</center></strong></h3>
          </div>
           <div class="panel-body" id="PanelBalanceBody">
               <!-- Column showing Current Balance and frozen balance of USD -->
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 10px">
                 <div class="panel panel-info">
                    <div class="panel-heading" style="height: 40px; padding-top: 2px">
                      <h4><b>USD</b></h4>
                    </div>
                    <div class="panel-body">
                      <!--Users current balance and frozen balance will be display here -->
                      <p style="margin-bottom:20px;">Current Balance <b id="moniC">650.00</b></p>
                      <p>Frozen Balance   <b id="moniF">10.88</b></p>
                      <!--Display of current balance and frozen balance ends here -->
                    </div>
                 </div>
              </div>
               <!-- Column showing Current Balance and frozen balance of GBP -->
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <div class="panel panel-info">
                    <div class="panel-heading" style="height: 40px; padding-top: 2px">
                      <h4><b>GBP</b></h4>
                    </div>
                    <div class="panel-body">
                      <!--Users current balance and frozen balance will be display here -->
                      <p style="margin-bottom:20px;">Current Balance <b id="moniC">650.00</b></p>
                      <p>Frozen Balance   <b id="moniF">10.88</b></p>
                      <!--Display of current balance and frozen balance ends here -->
                    </div>
                 </div>
              </div>
               <!-- Column showing Current Balance and frozen balance of EUR -->
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="panel panel-info">
                    <div class="panel-heading" style="height: 40px; padding-top: 2px">
                      <h4><b>EUR</b></h4>
                    </div>
                    <div class="panel-body">
                      <!--Users current balance and frozen balance will be display here -->
                      <p style="margin-bottom:20px;">Current Balance <b id="moniC">650.00</b></p>
                      <p>Frozen Balance   <b id="moniF">10.88</b></p>
                      <!--Display of current balance and frozen balance ends here -->
                    </div>
                 </div>
              </div>
               <!-- Column showing Current Balance and frozen balance of GHS -->
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                   <div class="panel panel-info">
                    <div class="panel-heading" style="height: 40px; padding-top: 2px">
                      <h4><b>GHS</b></h4>
                    </div>
                    <div class="panel-body">
                      <!--Users current balance and frozen balance will be display here -->
                      <p style="margin-bottom:20px;">Current Balance <b id="moniC">650.00</b></p>
                      <p>Frozen Balance   <b id="moniF">100.88</b></p>
                      <!--Display of current balance and frozen balance ends here -->
                    </div>
                 </div>
              </div>
           </div>
           <div class="panel-footer"></div>
      </div>
      <!-- Check Balance panel ends here -->
</div>
<!--Last column on page check balance -->
<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 hidden-sm hidden-xs">
</div>