 <!--=====================================
           ###......Account main page....#####
           - Button to confirm has Name (confirm) and id (confirm)

  =======================================-->

 <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0"></div>
         <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12" id="AccountPage">
           <!-- Beginning of form to process data -->
          <div class="form">
          <!-- Beginning of panel in Accounts page -->
          <div class="panel" id="AccountPanel">
          <!-- Panel Header -->
               <div class="panel-heading" id="AccountPanel_Header">
                
               </div>
          <!-- Panel Body -->
               <div class="panel-body" id="AccountPanel_Body">
                  <div class="col-lg-2 col-md-2 col-sm-0 col-xs-12"></div>
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <!-- Alert to prompt starts here -->
                    <div class="alert" id="alertConfirm">
                      <p>Creating Account for Kweku Manu</p>
                      <p id="ConfirmP">
                       <input type="button" class="btn btn-default" name="confirm" id="confirm" value="Confirm">
                      </p>
                    </div>
                    <!-- End of Alert -->
                    <!-- Alert to prompt starts here -->
                    <div class="alert" id="alertConfirm">
                      <p>Removing account of Kofi Manu 054TG5644DE98</p>
                      <p id="ConfirmP">
                       <input type="button" class="btn btn-default" name="confirm" id="confirm" value="Confirm">
                      </p>
                    </div>
                    <!-- End of Alert -->
                    <!-- Alert to prompt starts here -->
                    <div class="alert" id="alertConfirm">
                      <p>Suspending account of Kofi Manu 054TG5644DE98</p>
                      <p id="ConfirmP">
                       <input type="button" class="btn btn-default" name="confirm" id="confirm" value="Confirm">
                      </p>
                    </div>
                    <!-- End of Alert -->
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-0 col-xs-12"></div>
               </div>
          <!-- Panel Footer -->
               <div class="panel-footer" id="AccountPanel_Footer">
               <!--Button done starts here -->
                 <a href="#" class="btn" name="AccountDone" id="AccountDone">Done</a>
               </div>
          </div> 
          <!-- End of panel in accounts page -->
          </div>
        <!-- End of form -->
        </div>
        <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0"></div>