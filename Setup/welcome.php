<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
   <!-- Add title to the page -->
    <title>Prompt Network | Welcome</title>
    
    <!-- Adding the meta link -->
    <meta name="robots" content="noindex, nofollow" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     
    <!-- Add icon to the title -->
    <link rel="icon" type="image/png" href="../images/favicon.ico">

    <link rel="stylesheet" href="../Bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../Bootstrap/css/bootstrap-theme.min.css">

    <link rel="stylesheet" href="../Bootstrap/css/custom.css">
   <style type="text/css">
       /*Styling panel in welcome page */
    #welcome_Panel{
        font-family: "courier New", "Times New Romans", "Calibri";
        padding-top: 50px;
        padding-bottom: 50px;
        font-size: 20px;
    }
    
    #panel{
      margin-top: 100px;
      box-shadow: 0px 0px 10px 0px;
    }

    /* At media screen */
    @media (max-width: 768px) {
     #panel{
      margin-top:50px;
    }

    }
   </style>
</head>
<body>
     <!-- Navbar starts here -->
     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
           <a class="navbar-brand" href="index.php" id="title">Pr<span id="lo">o</span>mpt</a>
        </div>
      </div>
    </nav>
     <!-- Navbar ends here -->
        <!--First column starts here -->
        <div class="col-lg-3 col-md-3 col-sm-1 col-xs-12 hidden-xs">
           
        </div>
        <!--Middle Column Starts here -->
        <div class="col-lg-6 col-md-6 col-sm-10 col-xs-12">
             <!-- Panel starts here -->
              <div class="panel" id="panel">
                  <!-- Panel body starts here -->
                  <div class="panel-body" id="welcome_Panel">
                       <p><strong>Congratulations!!!</strong></p>
                       <p>Your <b>Bank Setup</b> has been Completed Successfully</p>
                       <!--Setup Bank vault id will be shown here -->
                       <p>Your <b>VAULT ID</b> is <b>GHN24369</b></p>
                       <P>Log in to your <a href="../engine1.php" id="display" data-toggle="tooltip" title="Click to Engine"><b>ENGINE</b></a> to finish your Settings</P>
                  </div>
                  <!-- /ends -->
              </div>
              <!-- Panel ends here -->
        </div>  
      <!-- Last Column starts here -->
      <div class="col-lg-3 col-md-3 col-sm-1 col-xs-12 hidden-xs"></div> 
   <!-- Start of footer -->
   <div class="container-fluid hidden-lg hidden-md">
      <footer class="navbar navbar-inverse navbar-fixed-bottom">
             <a href="index.html" id="arrowL" data-toggle="tooltip" title="Go Back">
               <span class="glyphicon glyphicon-circle-arrow-left"></span>
             </a> 
      </footer>
    </div> 
    <!-- /container -->    
     <!-- Applying tooltip styling in script -->
    <script>
       $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>
                   
</body>
</html>

















